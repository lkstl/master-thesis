\section{Enriched presheaves}
\subsection{Enriched functor categories}
In example \ref{example:classify-natural-trafos} we defined an object $\cat D^{\cat C}(F,G)$ of $\cat V$-natural transformations.
This object certainly exists if $\cat C$ is small and $\cat V$ is a cosmos, which we assume for the remainder of the section.
The notation suggests that there exists a $\cat V$-category $\cat D^{\cat C}$ with $\cat V$-functors from $\cat C$ to $\cat D$ as objects and the objects of $\cat V$-natural transformations as morphism objects.
Using the description of ends in terms of equalizers of products from the last section, the object $\cat D^{\cat C}(F,G)$ comes equipped with canonical projections $\epsilon_c$ to $D(Fc,Gc)$.
Alternatively, these are simply the components of the universal $\cat V$-extranatural transformation from $\cat C^{\cat D}(F,G)$ to $\cat D(F(-),G(-))$.
We obtain a composition map from the universal property of $\cat D^{\cat C}(F,H)$ by considering the diagram
\begin{equation*}
\begin{tikzcd}
  &[-18ex] \cat D(Gc',Hc')\otimes\cat D(Fc',Gc')
      \arrow[r, "\circ"]
  & \cat D(Fc',Hc')
      \arrow[rd]
  &[-8ex]
  \\
    \cat D^{\cat C}(G,H)\otimes\cat D^{\cat C}(F,G)
      \arrow[ru, "\epsilon_{c'}\otimes \epsilon_{c'}"{yshift=-4pt}]
      \arrow[rd, "\epsilon_c\otimes \epsilon_c"'{yshift=3.5pt}]
  &
  &
  & \cat D(Fc,Hc')^{\cat C(c,c')}
  \\
  & \cat D(Gc,Hc)\otimes\cat D(Fc,Gc)
      \arrow[r, "\circ"]
  & \cat D(Fc,Hc)
      \arrow[ru]
  &
\end{tikzcd}.
\end{equation*}
This defines a $\cat V$-extranatural transformation from $\cat D^{\cat C}(G,H)\otimes\cat D^{\cat C}(F,G)$ to the $\cat V$-functor $\cat D(F(-),H(-))$ and thus factors through $\cat D^\cat C(F,H)$:
\[\cat D^{\cat C}(G,H)\otimes\cat D^{\cat C}(F,G)\to\cat D^{\cat C}(F,H).\]
Similarly, for any $\cat V$-functor $F$ the family of identity arrows $(\abs{1_{Fc}})_c$ defines a $\cat V$-extranatural transformation which induces the $\cat V$-natural identity transformation $\abs{1_F}\colon 1_\otimes\to\cat D^{\cat C}(F,F)$.
Using the universal property of the object of $\cat V$-natural transformations we can check associativity and neutrality of identity arrows with respect to composition.
Thus, we have defined the $\cat V$-category of $\cat V$-functors $\cat D^{\cat C}$. 

The components of the canonical $\cat V$-extranatural transformation $\cat D^\cat C(F,G)\to\cat D(F(-),G(-))$ induce unique morphisms
\begin{equation*}
  \cat D^\cat C(F,G)\to\cat D(Fc,Gc')^{\cat C(c,c')}
\end{equation*}
for all objects $c$, $c'$ of $\cat C$.
Their exponential detranspositions become the morphism part of a canonical \emph{evaluation $\cat V$-functor} 
\begin{equation*}
  \cat D^\cat C\otimes \cat C\to\cat C
\end{equation*} 
sending a pair consisting of a $\cat V$-functor and an object of $\cat C$ to the value of the functor at the object.
Fixing a selector for a $\cat V$-natural transformation and an object $c$, the underlying functor of the induced functor $\cat D^\cat C\to\cat D^\cat C\otimes\cat C\to\cat D$ sends the transformation to its component at $c$.
Moreover via the evaluation $\cat V$-functor any $\cat V$-functor $Q\colon\cat C\to\cat E^\cat D$ can be transformed into a $\cat V$-bifunctor $\cat C\otimes\cat D\to\cat E$, the \emph{exponential detransposition} of $Q$:
\begin{equation*}
  \cat C\otimes\cat D\to[Q\otimes\cat D]\to\cat E^\cat D\otimes\cat D\to\cat E.
\end{equation*}

Conversely, any $\cat V$-bifunctor $P\colon\cat C\otimes\cat D\to\cat E$ induces a $\cat V$-functor $\cat C\to\cat E^\cat D$ which sends $c$ to $P(c,-)\colon\cat D\to\cat E$. 
Its action on morphisms is induced by the maps $P(-,d)_{c,c'}\colon\cat C(c,c')\to\cat E(P(c,d),P(c',d))$, for $d$ an object of $\cat D$.
The $\cat V$-extranaturality condition 
  \begin{equation*}
  \begin{tikzcd}[sep=scriptsize]
    &
    {\cat E(P(c,d'),P(c',d'))}
      \arrow[rd]
    & \\
    {\cat C(c,c)}
      \arrow[ru]
      \arrow[rd] 
    & &
    {\cat E(P(c,d),P(c',d'))^{\cat D(d,d')}}
    \\
    &
    {\cat E(P(c,d),(c',d))}
      \arrow[ru] 
    &
  \end{tikzcd}
  \end{equation*}
for this family of morphisms is seen to be equivalent to the commutativity of the diagram in figure \ref{diagram:extranaturality-currying}, after considering the adjoint diagram and some rearranging.
Similarly, $P$ induces a $\cat V$-functor $\cat D\to\cat E^\cat C$ which sends $d$ to the $\cat V$-functor $P(-,d)$.
These functors are called the \emph{exponential transpositions} of $P$ with respect to $\cat D$ respectively $\cat C$.
Exponential transposition and detransposition induce mutually inverse isomorphisms of categories between the category of $\cat V$-functors $\cat C\otimes\cat D\to\cat E$ and that of $\cat V$-functors $\cat C\to\cat E^{\cat D}$, see \cite[2.3]{Kelly2005basic}.

\begin{figure}
  \begin{tikzcd}[row sep={2cm,between origins}, column sep={3.2cm,between origins}, every cell/.append style={font=\tiny}] 
    & {\cat C(c,c')\otimes\cat D(d,d')} \arrow[ld, "\abs{1_c}\otimes 1 \otimes 1 \otimes\abs{1_d}"'] \arrow[rd, "1\otimes\abs{1_{d'}}\otimes\abs{1_c}\otimes 1"] &                                                                                                                                                  \\
{\cat C(c',c')\otimes\cat D(d,d')\otimes\cat C(c,c')\otimes\cat D(d,d)} \arrow[rd, "\circ\otimes\circ"'] \arrow[dd, "P\otimes P"'] &                                                                                                                                                                & {\cat C(c,c')\otimes\cat D(d',d')\otimes\cat C(c,c)\otimes\cat D(d,d')} \arrow[dd, "P\otimes P"] \arrow[ld, "\circ\otimes\circ"] \\
                                                                                                                                                   & {\cat C(c,c')\otimes\cat D(d,d')} \arrow[dd, "P" near start]                                                                                                      &                                                                                                                                                  \\
{\cat E(P(c',d),P(c',d'))\otimes\cat E(P(c,d),P(c',d))} \arrow[rd, "\circ"']                                                               &                                                                                                                                                                & {\cat E(P(c,d'),P(c',d'))\otimes\cat E(P(c,d),P(c,d'))} \arrow[ld, "\circ"]                                                              \\
                                                                                                                                                   & {\cat E(P(c,d),P(c',d'))}                                                                                                                                  &                                                                                                                                                 
  \end{tikzcd}
  \caption{The upper quadrilateral commutes by neutrality, the two lower ones by functoriality of $P$.}
  \label{diagram:extranaturality-currying}
\end{figure}

















\subsection{The strong Yoneda lemma and density formulae}
Our next goal is to study the functor $\cat V$-categories $\underline{\cat V}^{\cat C\op}$ and $(\underline{\cat V}^\cat C)\op$ for a $\cat V$-category $\cat C$.
Objects of $\underline{\cat V}^{\cat C\op}$ are called \emph{$\cat V$-presheaves}, while objects of $(\underline{\cat V}^{\cat C})\op$ are called \emph{$\cat V$-copresheaves}.
Applying exponential transposition to the enriched hom-bifunctor $\cat C\colon\cat C\op\otimes\cat C\to\underline{\cat V}$ produces two $\cat V$-functors
  \begin{gather*}
    \cat C\to\underline{\cat V}^{\cat C\op},
    \quad c\mapsto\cat C(-,c)\\
    \cat C\op\to\underline{\cat V}^{\cat C},
    \quad c\mapsto\cat C(c,-).
  \end{gather*}
The first $\cat V$-functor is called the \emph{Yoneda embedding} $\yoneda$ of $\cat C$ into its $\cat V$-category of enriched presheaves.
The second functor is nothing but the Yoneda embedding of $\cat C\op$.
We denote the dual $\cat V$-functor of the Yoneda embedding of $\cat C\op$ by $\opyoneda\colon\cat C\to(\underline{\cat V}^\cat C)\op$.
These $\cat V$-functors are \emph{fully faithful}, in that their morphism parts are isomorphisms in $\cat V$.
This is a consequence of the strong Yoneda lemma, we will discuss now. 

Given any $\cat V$-presheaf $X$ on $\cat C$, the Yoneda embedding of $\cat C$ enables us to construct a new $\cat V$-presheaf
\begin{equation*}
  \underline{\cat V}^{\cat C\op}(\yoneda(-),X)
  \coloneqq 
  \underline{\cat V}^{\cat C\op}(-,X)\circ\yoneda\op,
\end{equation*}
whose value on an object $d$ is the object of $\cat V$-natural transformations $\cat C(-,d)\to X$.
By (de)transposition, the action $X_{d,c}\colon\cat C(c,d)\to\underline{\cat V}(Xd,Xc)$ of $X$ on morphisms defines components of $\cat V$-extranatural transformations 
\begin{equation*}
  \{Xd\to\underline{\cat V}(\cat C(c,d),Xc)\}_c,
\end{equation*}
for all objects $d$ of $\cat D$.
Extranaturality boils down to functoriality of $X$ and can be proven by using a diagram quite similar to the one in figure \ref{diagram:extranaturality-currying}.
By the universal property of the object of $\cat V$-natural transformations, these $\cat V$-extranatural transformations in turn induce canonical morphisms
\begin{equation*}
  Xd\to\underline{\cat V}^{\cat C\op}(\cat C(-,d),X),
\end{equation*}
for all $d$.
One can show that the canonical morphisms are $\cat V$-natural in $d$ by using the universal property of the end that defines their codomain and subsequently chasing through a detransposed diagram.
Finally, the canonical morphisms are even isomorphisms for all $d$ and thus constitute the components of a $\cat V$-natural isomorphism.
Namely, for each $d$ we construct an inverse morphism 
  \begin{equation*}
  \underline{\cat V}^{\cat C\op}(\cat C(c,d),Xc)
    \to[p_d]
  \underline{\cat V}(C(d,d),Xd)
    \to[\underline{\cat V}(\abs{1_d},Xd)]
  \underline{\cat V}(1_\otimes, Xd)
    \cong
  Xd,
  \end{equation*}
which can be seen as a map that sends a natural transformation to its value at the identity of $d$.
This morphism, in fact, defines a morphism between $\cat V$-extranatural transformations to the $\cat V$-functor $\underline{\cat V}(\cat C(-,d),X(-))$, meaning that it is compatible with the maps $p_c$ and the (de)transpositions of $X_{d,c}$, for all $c$.
Once more, this can be checked by chasing through a detransposed diagram.
But then the universal property of the object of $\cat V$-natural transformations implies, that the composition
  \begin{equation*}
    \underline{\cat V}^{\cat C\op}(\cat C(c,d),Xc)
    \to
    Xd
    \to
    \underline{\cat V}^{\cat C\op}(\cat C(c,d),Xc)
  \end{equation*}
must equal the identity.
Finally, using the above compatibility assertion, it is not hard to show that the converse composition is also equal to the identity.
This proves the strong Yoneda lemma stated below.

\begin{proposition}[Strong Yoneda lemma]
  Given a $\cat V$-presheaf on a small $\cat V$-category $\cat C$, the canonical morphisms
  \begin{equation*}
    Xd\to\underline{\cat V}^{\cat C\op}(\cat C(-,d),X)=\underline{\cat V}^{\cat C\op}(\yoneda d,X)
  \end{equation*}
  assemble to a $\cat V$-natural isomorphism between $\cat V$-presheaves.
  Dually, there are canonical morphisms
  \begin{equation*}
    Yd\to\underline{\cat V}^\cat C(\cat C(d,-),Y)=(\underline{\cat V}^\cat C)\op(Y,\opyoneda d)
  \end{equation*}
  for any $\cat V$-copresheaf $Y$, which assemble to a $\cat V$-natural isomorphism between $\cat V$-copresheaves.
\end{proposition}
\begin{corollary}
  The Yoneda embeddings $\yoneda$ and $\opyoneda$ are fully faithful.
\end{corollary}

Combining the strong Yoneda lemma with the definition of the object of $\cat V$-natural transformations in example \ref{example:classify-natural-trafos}, we obtain, for any $\cat V$-presheaf $X$ and any $\cat V$-copresheaf $Y$, an end representation 
\begin{equation*}
  Xd\cong\int_c Xc^{\cat C(c,d)}
    \qquad\text{and}\qquad 
  Yd\cong\int_c Yc^{\cat C(d,c)}. 
\end{equation*}
There is also a dual version of this representation using coends which is called the density formula.
\begin{proposition}[Density formula]
  Let $X$ be a $\cat V$-presheaf and $Y$ be a $\cat V$-copresheaf on a small $\cat V$-category $\cat C$.
  There are canonical isomorphisms
  \begin{equation*}
    Xd\cong\int^c Xc\otimes\cat C(d,c)
      \qquad\text{and}\qquad 
    Yd\cong\int^c Yc\otimes\cat C(c,d),
  \end{equation*}
  which are $\cat V$-natural in $d$.
\end{proposition}
\begin{proof}
  There is a chain of isomorphisms which is $\cat V$-natural in $d$ and $v$:
  \begin{align*}
    \underline{\cat V}\left(\int^c Xc\otimes\cat C(d,c),v\right)
    &\cong\int_c\underline{\cat V}(Xc\otimes\cat C(d,c),v)\\
    &\cong\int_c\underline{\cat V}(\cat C(d,c),\underline{\cat V}(Xc,v))\\
    &\cong\underline{\cat V}^{\cat C}(\cat C(d,-),\underline{\cat V}(X(-),v))\\
    &\cong\underline{\cat V}(Xd,v).
  \end{align*}
  The first isomorphism holds by definition of the coend, the second one by exponential transposition, the third one by definition of the object of $\cat V$-natural transformations and the last one by the Yoneda lemma for $\cat V$-copresheaves.
  The conclusion then follows from the Yoneda embedding being full and faithful.
\end{proof}

These (co)end formulae hold objectwise and are indeed $\cat V$-natural in $d$ but we want to know wether they actually compute a (co)end in the $\cat V$-(co)presheaf $\cat V$-category.
Indeed, this is the case.
Like in ordinary category theory, one can compute (co)ends of $\cat V$-functors objectwise, as the following proposition shows.

\begin{proposition}\label{proposition:functor-category-ends}
  Let $\widehat{P}\colon\cat B\op\otimes\cat B\to\cat D^{\cat C}$ be a $\cat V$-functor, corresponding under exponentional detransposition to a $\cat V$-functor $P\colon\cat B\op\otimes\cat B\otimes\cat C\to\cat D$.
  Assuming the end $\int_b P(b,b,c)$ exists for all objects $c$ of $\cat C$, proposition \ref{proposition:parametrized-ends} gives us a $\cat V$-functor $\int_b P(b,b,-)\colon\cat C\to\cat D$.
  This functor is the end over $\widehat{P}$.
\end{proposition}
\begin{proof}
  Let $G$ be an arbitrary object of $\cat D^{\cat C}$.
  Using propositions \ref{proposition:parametrized-ends} and \ref{proposition:Fubini} we compute 
  \begin{align*}
  \cat D^{\cat C}(G,\int_b P(b,b,-))
  &\cong\int_c\cat D(Gc,\int_b P(b,b,c))\\
  &\cong\int_c\int_b\cat D(Gc,P(b,b,c))\\
  &\cong\int_b\int_c\cat D(Gc,P(b,b,c))\\
  &\cong\int_b\cat D^{\cat C}(G,P(b,b,-))\\
  &\cong\int_b\cat D^{\cat C}(G,\widehat{P}(b,b)).\qedhere
  \end{align*}
\end{proof}

Let us interpret these results in the light of the generalized space philosophy from the introduction.
Under the interpretation of the objects of $\cat C$ as simple geometric shapes and $\cat V$-presheaves as generalized spaces, the Yoneda embedding allows us to identify a shape $c$ with its associated generalized space $\cat C(-,c)$ whose parametrizations are simply the morphisms into $c$ in $\cat C$.
The morphisms in $\cat C$ can be viewed as incidence relations between shapes, so full faithfulness of the Yoneda embedding guarantees that these incidence relations are preserved under the identification.
Furthermore, the strong Yoneda lemma realizes the abstract parametrizations $Xc$ of a generalized space $X$ by a shape $c$ as genuine morphisms of generalized spaces $\cat C(-,c)\to X$.
Finally, the presentation of coends in terms of coequalizers identifies the density formula for a generalized space $X$ as a description for how to glue $X$ from its parametrizations.
There is an obvious dual version of this paragraph, using the interpretation of $\cat C$ as a category of algebraic quantity types and of $\cat V$-copresheaves as generalized algebras.







\subsection{Kan extension, nerve and realization}
\todo{Rework.}
Let $F\colon\cat C\to\cat E$ and $K\colon\cat C\to\cat D$ be $\cat V$-functors.
We want to extend $F$ along $K$ as in
\begin{equation*}
\begin{tikzcd}
\cat C
  \arrow[r, "F"]
  \arrow[d, "K"']
&
\cat E\\
\cat D
  \arrow[ru,dashed]
\end{tikzcd}.
\end{equation*}
Of course, it would be evil to demand commutativity of this triangle up to equality, it should rather commute up to $\cat V$-natural isomorphism.
Similarly, we would not want to ask for genuine uniqueness of such an extension but rather the datum of the extension together with the $\cat V$-natural isomorphism should be unique up to unique isomorphism.
Denoting the extension by $E$ and the $\cat V$-natural isomorphism by $\psi\colon F\to EK$, this means that for any other extension $E$ of $F$ by $K$ together with a $\cat V$-natural isomorphism $\psi'\colon F\to E'K$, there is a unique $\cat V$-natural isomorphism $\theta\colon E\isoto E'$ with $\theta\psi=\psi'$.
This is illustrated in the following diagram.
  \begin{equation*}
  \begin{tikzcd}[row sep = huge, column sep = large]
    \cat C 
      \arrow[rr, "F", ""'{name=F}, ""'{name=Fstart, near start}] 
      \arrow[d, "K"'] 
    && 
    \cat E
    \\
    \cat D
      \arrow[urr, "E'" description, ""{name=B, near start}, bend right=35, ""{name=Bmid}] 
      \arrow[Rightarrow, from=Fstart, start anchor={[xshift=-1ex]}, to=B, gray, "\psi'" near start, "\sim"'{near end, rotate=-70, scale=0.8, yshift=-0.5ex}, bend left =20]
      \arrow[urr, "E" description, ""{name=A, near start}, bend left=10, crossing over, ""'{name=Amid}]
      \arrow[Rightarrow, from=Fstart, start anchor={[xshift=-2ex]}, to=A, gray, "\psi"' near start, "\sim"{near start, rotate=-65, scale=0.8}, bend right=10]
      \arrow[Rightarrow, from=Amid, to=Bmid, gray, "\exists !\theta"]
  \end{tikzcd}
  \end{equation*}

There are more lenient versions of this universal extension property.
Namely, we could drop the demand of the $\cat V$-natural transformations to be isomorphisms.
In this case $E$ would be called the \emph{weak left Kan extension of $F$ along $K$}, denoted by $\Lan_K F$.
If we reverse the arrows of the $\cat V$-natural transformations in the above diagram we get the notion of a \emph{weak right Kan extension of $F$ along $K$}, denoted by $\Ran_K F$.
  \begin{equation*}
  \begin{tikzcd}[row sep = huge, column sep = large]
    \cat C 
      \arrow[rr, "F", ""'{name=F}, ""'{name=Fstart, near start}] 
      \arrow[d, "K"'] 
    && 
    \cat E
    \\
    \cat D
      \arrow[urr, "E'" description, ""{name=B, near start}, bend right=35, ""{name=Bmid}] 
      \arrow[Rightarrow, from=Fstart, start anchor={[xshift=-1ex]}, to=B, gray, "\eta'" near start, bend left =20]
      \arrow[urr, "\Lan_K F" description, ""{name=A, near start}, bend left=10, crossing over, ""'{name=Amid}]
      \arrow[Rightarrow, from=Fstart, start anchor={[xshift=-2ex]}, to=A, gray, "\eta"' near start, bend right=10]
      \arrow[Rightarrow, from=Amid, to=Bmid, gray, "\exists !\theta"]
  \end{tikzcd}
  ,\qquad
  \begin{tikzcd}[row sep = huge, column sep = large]
    \cat C 
      \arrow[rr, "F", ""'{name=F}, ""'{name=Fstart, near start}] 
      \arrow[d, "K"'] 
    && 
    \cat E
    \\
    \cat D
      \arrow[urr, "E'" description, ""{name=B, near start}, bend right=35, ""{name=Bmid}] 
      \arrow[Leftarrow, from=Fstart, start anchor={[xshift=-1ex]}, to=B, gray, "\varepsilon'" near start, bend left =20]
      \arrow[urr, "\Ran_K F" description, ""{name=A, near start}, bend left=10, crossing over, ""'{name=Amid}]
      \arrow[Leftarrow, from=Fstart, start anchor={[xshift=-2ex]}, to=A, gray, "\varepsilon"' near start, bend right=10]
      \arrow[Leftarrow, from=Amid, to=Bmid, gray, "\exists !\theta"]
  \end{tikzcd}
  \end{equation*}
We call the associated $\cat V$-natural transformations $\eta\colon F\to\Lan_KFK$, respectively $\varepsilon\colon \Ran_KFK\to F$, the \emph{unit} respectively \emph{counit} of the Kan extension.
They are uniquely defined by the isomorphisms
\begin{gather*}
  \cat V(1_\otimes,\cat E^{\cat C}(TK,F))
  \cong
  \cat V(1_\otimes,\cat E^{\cat D}(T,\Ran_KF)),\\
  \cat V(1_\otimes,\cat E^{\cat C}(F,TK))
  \cong
  \cat V(1_\otimes,\cat E^{\cat D}(\Lan_KF,T)),
\end{gather*}
natural in $T$.
It is obvious how to define enriched Kan extensions, simply drop the underlying elements functor $\cat V(1_\otimes,-)$ and demand a $\cat V$-natural isomorphism
\begin{gather*}
  \cat E^{\cat C}(TK,F)
  \cong
  \cat E^{\cat D}(T,\Ran_KF),\\
  \cat E^{\cat C}(F,TK))
  \cong
  \cat E^{\cat D}(\Lan_KF,T)).
\end{gather*}

Using coends and ends, we can construct right (left) Kan extensions into (co)complete categories $\cat E$.
\begin{proposition}\label{proposition:Kan-formula}
  Let $F\colon\cat C\to\cat E$ and $K\colon\cat C\to\cat D$.
  If the (co)ends and (co)powers involved in the following formulas exist, they compute the (left) right Kan extension of $F$ along $K$:
  \begin{equation*}
  \Ran_K F
  \cong
  \int_c Fc^{\cat D(-,Kc)},
  \qquad
  \Lan_K F
  \cong
  \int^c \cat D(Kc,-)\otimes Fc.
  \end{equation*}
\end{proposition}
\begin{proof}
  We will only show the left Kan extension.
  \begin{align*}
  \cat E^{\cat D}\left(\int^c\cat D(Kc,-)\otimes Fc,T\right)
  &\cong\int_d\cat E\left(\int^c\cat D(Kc,d)\otimes Fc,Td\right)\\
  &\cong\int_d\int_c\cat E(\cat D(Kc,d)\otimes Fc,Td)\\
  &\cong\int_c\int_d\underline{\cat V}(\cat D(Kc,d),\cat E(Fc,Td))\\
  &\cong\int_c\underline{\cat V}^{\cat D}(\cat D(Kc,-),\cat E(Fc,T(-)))\\
  &\cong\int_c\cat E(Fc,TKc)\\
  &\cong\cat E^{\cat C}(F,TK).\qedhere
  \end{align*}
\end{proof}

Kan extensions which admit a (co)end representation are called pointwise.
In the following we will only be concerned with Kan extensions of this type, and will therefore suppress the adjective ``pointwise'' right away.
Since we are still interested in genuine extensions, not just their left and right approximations in form of Kan extensions, it is useful to know when a Kan extension is an extension.
This is the case for extension along a full and faithful functor, as the following lemma shows.
\begin{lemma}\label{lemma:Kan-fully-faithful}
  Let $F\colon\cat C\to\cat E$, $K\colon\cat C\to\cat D$ be $\cat V$-functors and suppose that $K$ is fully faithful.
  In this case, a Kan extension of $F$ along $K$ is a genuine extension, meaning that the unit $\eta\colon F\to\Lan_K FK$, respectively the counit $\varepsilon\colon\Ran_K FK\to F$, is an isomorphism.
\end{lemma}
\begin{proof}
  We have a $\cat V$-natural isomorphism
  \begin{equation*}
  \Lan_KFK=\int^c\cat D(Kc,K(-))\otimes Fc\cong\int^c\cat C(c,-)\otimes Fc.
  \end{equation*}
  The right side is a left Kan extension of $F$ along the identity $\cat V$-functor on $\cat C$, which is just $F$ itself.
\end{proof}

Applying this lemma to the case that $K$ is the Yoneda embedding, we obtain the main theorem for the $\cat V$-category of $\cat V$-(co)presheaves.
\begin{theorem}[Nerve and realization]
  Let $F\colon\cat C\to\cat E$ be a $\cat V$-functor into a cocomplete $\cat V$-category.
  There is a unique extension $\re_F\colon\underline{\cat V}^{\cat C\op}\to\cat E$ of $F$ along $\yoneda$, up to unique isomorphism.
  Moreover, $\re_F$ is left adjoint to a $\cat V$-functor $\N_F$.
  \begin{equation*}
  \begin{tikzcd}[sep=large]
    \cat C
      \arrow[r, "F"]
      \arrow[d, "\yoneda"']
    &
    \cat E
      \arrow[dl, bend right=20, "\N_F"'{name=N,xshift=1pt,yshift=-2pt}]
    \\
    \underline{\cat V}^{\cat C\op}
      \arrow[ur, bend right=20, "\re_F"'{name=R}]
    \arrow[phantom,from=N,to=R,"\vdash"{rotate=-45}]
  \end{tikzcd}
  \end{equation*}
\end{theorem}
\begin{proof}
  Since $\cat E$ is cocomplete, the left Kan extension of $F$ along $\yoneda$ exists and can be computed by the coend formula in proposition \ref{proposition:Kan-formula}.
  By lemma \ref{lemma:Kan-fully-faithful} this left Kan extension is a genuine extension of $F$ along $\yoneda$, so we set $\re_F\coloneqq\Lan_\yoneda F$.
  Furthermore, by defining $N_F$ as $e\mapsto\cat E(F(-),e)$, we see that
  \begin{align*}
  \cat E(\re_F(X),e)
  &=\cat E\left(\int^c\underline{\cat V}^{\cat C\op}(\yoneda c,X)\otimes Fc,e\right)\\
  &\cong\int_c\cat E(\underline{\cat V}^{\cat C\op}(\yoneda c,X)\otimes Fc,E)\\
  &\cong\int_c\cat E(Xc\otimes Fc,e)\\
  &\cong\int_c\underline{\cat V}(Xc,\cat E(Fc,e))\\
  &\cong\underline{\cat V}^{\cat C\op}(X,\cat E(F(-),e))\\
  &=\underline{\cat V}^{\cat C\op}(X,N_Fe),
  \end{align*}
  so $\re_F$ is left adjoint to $N_Fe$.
\end{proof}

Using the density formula for a $\cat V$-presheaf $X$, we see that there was no other choice for the definition of $\re_F$ other than by the pointwise coend formula for left Kan extensions if we want $\re_F$ to be left adjoint and thus coend and copower preserving%
\footnote{That left adjoint $\cat V$-functors preserve colimit constructions follows for the exact same formal reasons as in ordinary category theory.}:
\begin{align*}
  \re_F(X)&\cong\re_F\left(\int^c Xc\otimes\yoneda c\right)\\
          &\cong\int^cXc\otimes\re_F\yoneda c\\
          &\cong\int^c Xc\otimes Fc\\
          &\cong\int^c \underline{\cat V}^{\cat C\op}(\yoneda c,X)\otimes Fc.
\end{align*}
In particular, we could have used this as a definition for $\re_F$, while only demanding of it to be copower- and coend-preserving, and still gotten a right adjoint $N_F$. 
This also explains the name for the density formula: By the nerve-and-realization theorem, a left adjoint $\cat V$-functor on the $\cat V$-category of $\cat V$-presheaves on the $\cat V$-category $\cat C$ is already uniquely determined (up to isomorphism) by its value on $\cat C$ because we can present all $\cat V$-presheaves as a coend of represented presheaves $\yoneda c$.

\begin{corollary}[Isbell conjugation]
  There is a canonical adjunction between the $\cat V$-category of $\cat V$-presheaves and the $\cat V$-category of $\cat V$-copresheaves
  \begin{equation*}
  \begin{tikzcd}[sep=large]
    \cat C
      \arrow[r, "\opyoneda"]
      \arrow[d, "\yoneda"']
    &
    (\underline{\cat V}^{\cat C})\op
      \arrow[dl, bend right=20, "\Spec"'{name=N,xshift=1pt,yshift=-2pt}]
    \\
    \underline{\cat V}^{\cat C\op}
      \arrow[ur, bend right=20, "\cat O"'{name=R}]
    \arrow[phantom,from=N,to=R,"\vdash"{rotate=-45}]
  \end{tikzcd}
  \end{equation*}
\end{corollary}
\begin{proof}
  The nerve-and-realization theorem is applicable because by proposition \ref{proposition:functor-category-ends}, a $\cat V$-functor category is complete if the target category is.
  This is certainly the case for $\underline{\cat V}^{\cat C}$.
\end{proof}
There is an obvious dual approach to Isbell conjugation by developing a dual nerve-and-realization theorem for $\opyoneda$.
The resulting adjunction stays the same.

