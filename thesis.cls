\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{thesis}
\DeclareOption{draft}{
  \PassOptionsToClass{\CurrentOption}{scrartcl}
  \PassOptionsToPackage{\CurrentOption}{hyperref}
}
\ProcessOptions\relax
\LoadClass[oneside]{amsart}

\RequirePackage{thesis}
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage[protrusion=true,expansion=true]{microtype}
\RequirePackage[USenglish]{babel}
\RequirePackage[color=gray!40,obeyDraft]{todonotes}
\RequirePackage{enumitem}
\PassOptionsToClass{pdftex}{hyperref}
\RequirePackage[hypertexnames=false]{hyperref}

\theoremstyle{plain}
\newtheorem{theorem}{Theorem}[section]
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\theoremstyle{definition}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{remark}[theorem]{Remark}
\newtheorem{example}[theorem]{Example}
