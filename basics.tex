\section{Basics of enriched category theory}
  The idea of enriched category theory is to replace the hom sets of a category by objects of an arbitrary category $\cat V$.
  For this to make sense, the category $\cat V$ must be endowed with additional structure which allows us to interpret composition and identity arrows internal to $\cat V$.
Namely, $\cat V$ should carry a \emph{monoidal product} $\otimes\colon\cat V\times\cat V\to\cat V$ and a \emph{monoidal unit} $1_\otimes:1\to\cat V$ ($1$ denotes the terminal category, so $1_\otimes$ is determined by the choice of an object in $\cat V$).
Furthermore the monoidal product is required to be associative and the monoidal unit should be neutral with respect to the monoidal product.
As a categorification of the corresponding axioms of a monoid, these conditions are not imposed as equalities but are rather required to hold only up to natural isomorphism, encoded by \emph{associators} $\alpha_{u,v,w}\colon (u \otimes v)\otimes w \isoto u\otimes (v\otimes w)$ and \emph{left} and \emph{right unitors} $\lambda_v\colon 1_\otimes\otimes v\isoto v$, $\rho_v\colon v\otimes 1_\otimes\isoto v$, natural in $u,v,w$.
These isomorphisms in turn should satisfy certain coherence conditions in the form of commutative diagrams, guaranteeing that iterated monoidal products which only differ by bracketing and occurences of the monoidal unit are related by a unique isomorphism.
See \cite[chapter VII]{MacLane1998categories} for more details.
In the following discussion of the basic concepts of enriched category theory, we usually suppress unitors and associators.
Along the way we will refine our base category $\cat V$ slightly, to allow for more constructions and flexibility.

\begin{example}
  In a category with finite products, any choice of binary product for each pair of objects determines a bifunctor from the category to itself.
  Together with the terminal object as unit, this defines a monoidal structure on the category.
  Monoidal structures that arise in this fashion are called \emph{cartesian}.
\end{example}

\subsection{Enriched categories}
  Let $\cat V$ be a monoidal category.
  A \emph{$\cat V$-category} $\cat C$ (also called an \emph{enriched} category over $\cat V$) consists of a class of objects, a hom object $\cat C(x,y)$ in $\cat V$ for each pair of objects $x,y$, a composition morphism
  \[\circ_{x,y,z}\colon\cat C(y,z)\otimes\cat C(x,y)\to\cat C(x,z)\]
  for each triple of objects $x,y,z$, and a morphism $\abs{1_x}\colon 1_\otimes\to\cat C(x,x)$ for each object $x$, the selection of the identity arrow at $x$.
  Furthermore, the composition is associative and the identity arrows act neutrally with respect to composition, in the sense that the following diagrams commute for all objects $w,x,y,z$ of $\cat C$:
  \begin{equation}\label{diagram:composition-associativity}
  \begin{tikzcd}
    {\cat C(y,z)\otimes\cat C(x,y)\otimes\cat C(w,x)}
      \arrow[r, "{1\otimes\circ}"]
      \arrow[d, "{\circ\otimes 1}"]
    &
    {\cat C(y,z)\otimes\cat C(w,y)}
      \arrow[d, "{\circ}"]
    \\
    {\cat C(x,z)\otimes\cat C(w,x)}
      \arrow[r, "{\circ}"]
    &
    {\cat C(w,z)}
  \end{tikzcd}
  \end{equation}
  \begin{equation} 
  \begin{tikzcd}
    \cat C(x,y)\otimes 1_\otimes
      \arrow[d, "1\otimes\abs{1_x}"']
      \arrow[dr, "\rho"]
    &&
    1_\otimes\otimes\cat C(x,y)
      \arrow[d, "\abs{1_y}\otimes 1"]
      \arrow[dl, "\lambda"']
    \\
    \cat C(x,y)\otimes\cat C(x,x)
      \arrow[r, "\circ"']
    &
    \cat C(x,y)
    &
    \cat C(y,y)\otimes\cat C(x,y)
      \arrow[l, "\circ"]
  \end{tikzcd}
  \end{equation}

\begin{example}
  Enriched categories over the category of sets with cartesian monoidal structure are exactly the locally small categories.
  We assume all ordinary categories to be locally small, unless stated otherwise.
  In the following, we will discuss several enriched versions of concepts that are familiar from ordinary category theory. 
  If the enriching category $\cat V$ is chosen as the category of sets, the enriched concepts will reproduce their ordinary counterparts.
\end{example}


For each $\cat V$-category $\cat C$ we would like to construct its \emph{dual category} $\cat C\op$ with the same class of objects but $\cat C\op(x,y)\coloneqq\cat C(y,x)$ as hom objects.
Obviously, the identity arrows can stay unchanged.
However, to define a composition operation, we need extra structure on $\cat V$, a \emph{braiding}.
This is an isomorphism 
  \[\tau_{u,v}\colon u\otimes v\isoto v\otimes u\]
natural in $u,v$, such that certain coherence conditions with respect to the associator of $\cat V$ are satisfied.
We can then set
  \[\cat C(z,y)\otimes\cat C(y,x)\to[\tau]\cat C(y,x)\otimes\cat C(z,y)\to[\circ]\cat C(z,x)\]
as composition for the dual category.
Similarly the braiding allows us to define a composition operation for the \emph{monoidal product} $\cat C\otimes\cat D$ of two $\cat V$-categories $\cat C$ and $\cat D$, whose objects are pairs of objects from $\cat C$, $\cat D$ and whose morphisms are given by the monoidal product $\cat C(c,c')\otimes\cat D(d,d')$.
For convenience, we assume the braiding to be \emph{symmetric}, that is, $\tau\cdot\tau$ should be equal to the identity transformation.
There is a coherence theorem, like for monoidal categories, guaranteeing that all formal diagrams involving the braiding, associators and unitors commute, see \cite[chapter XI]{MacLane1998categories}.

We want the enriching symmetric monoidal category $\cat V$ to be enriched over itself.
To this end, we assume $\cat V$ to be \emph{closed} with respect to its symmetric monoidal structure, that is, each functor $-\otimes v$ is assumed to possess a right adjoint $(-)^v$.
Its value at an object $w$ is called the \emph{exponential} or \emph{internal hom}  $w^v$ from $v$ to $w$.
The natural bijection associated to this adjunction,
\begin{equation*}
  \cat V(u\otimes v,w)\isoto\cat V(u,w^v),
\end{equation*}
we call \emph{exponential transpositon} and its inverse \emph{exponential detransposition}. 
More generally we use the (de)transposition terminology for any natural bijection between hom sets associated to an adjunction.
The fact that $-\otimes v$ is part of a bifunctor lets us extend the exponential to a unique bifunctor $\cat V\op\times\cat V\to\cat V$ up to natural isomorphism, see \cite[Theorem IV.7.3]{MacLane1998categories}.
The counit of the adjunction is called \emph{evaluation}
  \[\ev_{u,v}\colon v^u\otimes u\to v\]
and it allows us to define a composition operation 
  $w^v\otimes v^u\to w^u$
as the exponential transpose of 
  \[w^v\otimes v^u\otimes u
      \to[w^v\otimes \ev_{u,v}]
    w^v\otimes v
      \to[\ev_{v,w}]
    w.\]
This terminology is motivated by the fact that the set of morphisms $1_\otimes\to v^u$ is in bijection with the set of morphisms $u\to v$ by exponential transposition (see also subsection \ref{subsection:underlying-category}).
Together with the transposes of left unitors $1_\otimes\otimes v\to v$ as identity arrows $1_\otimes\to v^v$, the symmetric monoidal closed category $\cat V$ becomes enriched over itself.
To avoid confusion, we will denote this enriched category by $\underline{\cat V}$.
Thus, $\underline{\cat V}(v,w)$ is the internal hom $w^v$, an object of $\cat V$, while $\cat V(v,w)$ remains the external hom, a set.
  
Let us prove associativity of composition for $\underline{\cat V}$ as an example.
We will use the following widely applicable technique which follows from uniqueness of (de)transposes (in this case, with respect to the exponential adjunction): Two morphisms are equal if and only if they are equal after (de)transposition.
In particular, a diagram commutes if and only if the (de)transposed diagram commutes.
Explicitly this means that we have to tensor the associativity diagram $\eqref{diagram:composition-associativity}$ (for $\cat C(x,y)=\underline{\cat V}(x,y)=y^x$) with $w$ and postcompose with evaluation.
Figure \ref{figure:associativity} shows that this yields a commutative diagram.

\begin{figure}
  \begin{tikzcd}[column sep={1.75cm,between origins}, row sep={1.55cm,between origins}]
                                          &  &                                                                                               &                                                                                                                                                                   & z                                         &                                                                                             &                                          \\
z^y\otimes y \arrow[rrrru, "\ev"] &  &                                                                                               & z^y\otimes y^x\otimes x \arrow[lll, "z^y\otimes\ev"] \arrow[rrr, "\circ\otimes x"']                                                                       &                                           &                                                                                             & z^x\otimes x \arrow[llu, "\ev"'] \\
                                          &  &                                                                                               & z^y\otimes y^x\otimes x^w\otimes w \arrow[u, "z^y\otimes y^x\otimes\ev"] \arrow[ld, "z^y\otimes\circ\otimes w"'] \arrow[rrd, "\circ\otimes x^w\otimes w"{xshift=2ex, yshift=-1ex}] &                                           &                                                                                             &                                          \\
                                          &  & z^y\otimes y^w\otimes w \arrow[lluu, "z^y\otimes\ev"] \arrow[rrdd, "\circ\otimes w"'] &                                                                                                                                                                   &                                           & z^x\otimes x^w\otimes w \arrow[ldd, "\circ\otimes w"] \arrow[ruu, "z^x\otimes\ev"'] &                                          \\
                                          &  &                                                                                               &                                                                                                                                                                   &                                           &                                                                                             &                                          \\
                                          &  &                                                                                               &                                                                                                                                                                   & z^w\otimes w \arrow[uuuuu, crossing over, "\ev"{yshift=20ex}] &                                                                                             &                                         
  \end{tikzcd}
  \caption{
    Commutative subdivisioned tetrahedron showing the associativity of composition in $\underline{\cat V}$.
    The front quadrilateral in the bottom face of the tetrahedron is the associativity diagram for $\underline{\cat V}$ tensored with $w$.
    The faces with apex $z$ commute by definition of composition.
    The same holds for the left quadrilateral in the bottom face.
    The right quadrilateral commutes by functoriality of the monoidal product.}
  \label{figure:associativity}
\end{figure}

\begin{example}
  A category which is closed with respect to a cartesian monoidal structure is called \emph{cartesian closed}.
  Notable examples of cartesian closed categories are the category of sets, the category of set-valued presheaves on any small category and reflective subcategories of cartesian closed categories with product-preserving reflector, see \cite[A.4.3.1]{Johnstone2002sketches}.
  This includes all Grothendieck toposes, their sheafification functor preserves all finite limits (in fact, they are exactly the left exact localizations of presheaf toposes, see \cite[C.2.1.11]{Johnstone2002sketches}), but also the category of small categories, which are reflectively embedded in the category of simplicial sets via the nerve functor.
  A category enriched over the category of small categories is a (locally small) \emph{strict 2-category}.
\end{example}

\subsection{Enriched functors}
  Let $\cat V$ be a monoidal category.
  For two $\cat V$-categories $\cat C$, $\cat D$, a \emph{$\cat V$-functor} $F\colon\cat C\to\cat D$ is given by a map that sends objects $x$ of $\cat C$ to objects $Fx$ of $\cat D$, together with morphisms
  \[\cat C(x,y)\to[F_{x,y}]\cat D(Fx,Fy)\]
for all pairs of objects $x,y$ in $\cat C$.
Furthermore, $F$ is required to respect composition and identity arrows, that is, the following diagrams must commute for all objects $x,y,z$ of $\cat C$:
  \begin{equation*}
  \begin{tikzcd}[column sep=huge]
    \cat C(y,z)\otimes\cat C(x,y)
      \arrow[r, "F_{y,z}\otimes F_{x,y}"]
      \arrow[d, "\circ"]
    &
    \cat D(Fy,Fz)\otimes\cat D(Fx,Fy)
      \arrow[d, "\circ"]
    \\
    \cat C(x,z)
      \arrow[r, "F_{x,z}"]
    &
    \cat D(Fx,Fz)
  \end{tikzcd}
  \end{equation*}
  \begin{equation*}
  \begin{tikzcd}
    &
    1_\otimes
      \arrow[dl, "\abs{1_x}"']
      \arrow[dr, "\abs{1_{Fx}}"]
    &[-2ex]
    \\
    \cat C(x,x)
      \arrow[rr, "F_{x,x}"]
    &&
    \cat D(Fx,Fx) 
  \end{tikzcd}
  \end{equation*}

\begin{example}
  Let $\cat V$ by symmetric monoidal.
  To any $\cat V$-category $\cat C$ we can associate an \emph{enriched hom-bifunctor} $\cat C\op\otimes\cat C\to\underline{\cat V}$ which sends a pair $(x,y)$ to the hom object $\cat C(x,y)$.
  By abuse of notation this functor is called $\cat C$ as well.
  Its action on morphisms
  \begin{equation*}
    (\cat C\op\otimes\cat C)((x,y)(x',y'))
      =\cat C(x',x)\otimes\cat C(y,y')
      \to[\cat C_{(x,y),(x',y')}]
      \underline{\cat V}(\cat C(x,y),\cat C(x',y'))
  \end{equation*}
  is given by the exponential transpose of one of the two outer paths in the following commutative diagram:
    \begin{center}
    \begin{tikzcd}[column sep=large]
      {\cat C(x',x)\otimes\cat C(y,y')\otimes\cat C(x,y)}
        \arrow[r, "1\otimes\circ"]
        \arrow[d, "\tau"]
        &
      {\cat C(x',x)\otimes\cat C(x,y')}
        \arrow[d, "\tau"]
        \\
      {\cat C(y,y')\otimes\cat C(x,y)\otimes\cat C(x',x)}
        \arrow[r, dashed, "\circ\otimes 1"]
        \arrow[d, "1\otimes\circ"]
        &
      {\cat C(x,y')\otimes\cat C(x',x)}
        \arrow[d, "\circ"]
        \\
      {\cat C(y,y')\otimes\cat C(x',y)}
        \arrow[r, "\circ"]
        &
      {\cat C(x',y')}
    \end{tikzcd}
    \end{center}
  The dashed arrow makes the upper part a naturality square and the lower part commutes by associativity of composition, so the whole diagram commutes.
\end{example}

There are various constructions available for $\cat V$-functors:
\begin{enumerate}
\item Two $\cat V$-functors $F\colon\cat C\to\cat D$, $G\colon\cat D\to\cat E$ can be composed to a $\cat V$-functor $GF$, sending $c$ to $GFc$ with morphism part
\begin{equation*}
  \cat C(c,c')\to[F_{c,c'}]\cat D(Fc,Fc')\to[G_{Fc,Fc'}]\cat E(GFc,GFc').
\end{equation*}

\item Any $\cat V$-functor $F\colon\cat C\to\cat D$ induces a dual $\cat V$-functor $F\op\colon\cat C\op\to\cat D\op$ between the corresponding dual categories, which acts like $F$ on objects and has $F\op_{x,y}\coloneqq F_{y,x}$ as morphism maps.

\item We can also form the \emph{external monoidal product} of $F$ with another $\cat V$-functor $F'\colon\cat C'\to\cat D'$ and obtain a $\cat V$-functor $F\otimes F'\colon\cat C\otimes\cat C'\to\cat D\otimes\cat D'$.
This $\cat V$-functor sends pairs of objects $(c,c')$ to $(Fc,Fc')$ and acts on morphisms by $(F\otimes F')_{(x,x'),(y,y')}\coloneqq F_{x,y}\otimes F'_{x',y'}$.

\item Given $\cat V$-categories $\cat C, \cat D$ and a fixed object $d\in\cat D$, there is a $\cat V$-functor $(-,d)\colon\cat C\to\cat C\otimes\cat D$ sending $c$ to $(c,d)$ with morphism part
  \begin{equation*}
    \cat C(c,c')
      \cong 
    \cat C(c,c')\otimes 1_\otimes
      \to[1\otimes\abs{1_d}]
    \cat C(c,c')\otimes\cat C(d,d)
  \end{equation*}
Similarly any fixed object $c$ of $\cat C$ induces a $\cat V$-functor $(c,-)\colon\cat D\to\cat C\otimes\cat D$.
\end{enumerate}

Combining the first and the last construction, every $\cat V$-bifunctor $P\colon\cat C\otimes\cat D\to\cat E$ induces $\cat V$-functors $P(-,d)\colon\cat C\to\cat E$ and $P(c,-)\colon\cat D\to\cat E$ for all objects $c$ of $\cat C$ and $d$ of $\cat D$.
This is useful for the next example.

\begin{example}
The enriched hom-bifunctor induces \emph{covariant} and \emph{contravariant enriched represented functors}
  \begin{equation*}
    \cat C(c,-)\colon\cat C\to\cat C\op\otimes\cat C\to[\cat C]\underline{\cat V},
    \qquad
    \cat C(-,c)\colon\cat C\op\to\cat C\op\otimes\cat C\to[\cat C]\underline{\cat V}
  \end{equation*}
for any fixed object $c$ of $\cat C$.
Their morphism part is transposed to composition 
\begin{equation*}
  \cat C(y,y')\otimes \cat C(c,y)\to[\circ]\cat C(c,y'),
  \qquad
  \cat C\op(x,x')\otimes\cat C(x,c)\to[\circ\cdot\tau]\cat C(x',c).
\end{equation*}
\end{example}














\subsection{The underlying category of an enriched category}\label{subsection:underlying-category}
Let $\cat C$ be a $\cat V$-category over a monoidal category $\cat V$. A morphism $1_\otimes\to\cat C(x,y)$ in $\cat V$ can be interpreted as a selection of an abstract arrow $f\colon x\to y$ in the enriched category $\cat C$.
Therefore we call this morphism a \emph{selector} of $f$ and denote it by $\abs{f}$, like in the case of identity arrows in the definition of an enriched category.
The collections $\abs{\cat C}(x,y)\coloneqq \cat V(1_\otimes,\cat C(x,y))$ of selectors for abstract morphisms $x\to y$ form the hom sets for a category $\abs{\cat C}$, the \emph{underlying category} of $\cat C$.

\begin{remark}
Recall that a functor $L\colon\cat U\to\cat V$ between two monoidal categories is \emph{lax monoidal} if it is equipped with a natural transformation $L(-)\otimes L(-)\to L(-\otimes-)$, called an \emph{Eilenberg--Zilber transformation}, and a morphism $1_\otimes\to L(1_\otimes)$ satisfying certain compatibility laws with respect to the associators and unitors of the two categories. 
In the presence of $L$, one can associate to any $\cat U$-category $\cat C$ a $\cat V$-category with the same objects but with hom objects $L\cat C(x,y)$.
The structure maps $L\cat C(y,z)\otimes L\cat C(x,y)\to L(\cat C(y,z)\otimes\cat C(x,y))$  and  $1_\otimes\to L(1_\otimes)$ allow us to pull back the images of the composition maps and identity arrows to define a $\cat V$-category $L\cat C$.
The coherence conditions for $L$ suffice to infer associativity and neutrality for composition over $\cat V$ from the corresponding properties over $\cat U$.
An example of a lax monoidal functor is the represented hom functor $\cat V(1_\otimes,-)$ to the category of sets with its cartesian monoidal structure, giving rise to the underlying category of a $\cat V$-category.
\end{remark}

In the following, let $\cat V$ be symmetric monoidal.
\begin{example}
  The underlying category of the dual of a $\cat V$-category is isomorphic to the dual of its underlying category, that is, $\abs{\cat C\op}\cong\abs{\cat C}\op$.
\end{example}
\begin{example}\label{example:underlying-monoidal-product}
  The underlying category of the monoidal product of two $\cat V$-categories $\cat C$, $\cat D$ is \emph{not} isomorphic to the product of their underlying categories.
  However, there is a canonical functor
  \begin{equation*}
    \abs{\cat C}\times\abs{\cat D}\to\abs{\cat C\otimes\cat D}
  \end{equation*}
  which is the identity on objects and sends a pair of selectors $(\abs{f},\abs{g})$, say for morphisms $f\colon c\to c'$ in $\cat C$ and $g\colon d\to d'$ in $\cat D$, to the selector
  \begin{equation*}
    1_\otimes
      \cong 
    1_\otimes\otimes 1_\otimes
      \to[\abs{f}\otimes\abs{g}]
    \cat C(c,c')\otimes\cat D(d,d').
  \end{equation*}
  The action of this functor on morphisms is exactly given by components of the Eilenberg--Zilber transformation of the lax monoidal functor $\cat V(1_\otimes,-)$, which guarantees that this action is in fact functorial.
\end{example}

\begin{example}
  Suppose $\cat V$ is symmetric monoidal closed and thus enriched over itself.
  The underlying category $\abs{\underline{\cat V}}$ of $\underline{\cat V}$ is isomorphic to $\cat V$ as an ordinary category.
  A functorial isomorphism is given by the identity on objects and by the bijection
  \begin{equation*}
    \abs{\underline{\cat V}}(u,v)
      =\cat V(1_\otimes,\underline{\cat V}(u,v))
      =\cat V(1_\otimes, v^u)
      \cong \cat V(1_\otimes \otimes u,v)
      \cong \cat V(u,v),
  \end{equation*}
  using exponential transposition, on morphisms.
  Under this isomorphism any selector $\abs{f}\colon 1_\otimes\to\underline{\cat V}(u,v)$ of an abstract morphism $f\colon u\to v$ in $\underline{\cat V}$ corresponds to an ordinary morphism $\tilde{f}\colon u\to v$ in $\cat V$.
  Namely, $\tilde{f}$ is the exponential detranspose of the selector up to a unitor, $\tilde{f}=\ev\cdot(\abs{f}\otimes u)\cdot\lambda^{-1}$.
  For this reason we do not have to distinguish between abstract morphisms in the enriched category $\underline{\cat V}$ and ordinary morphisms in the ordinary category $\cat V$.
\end{example}


Any $\cat V$-functor $F\colon\cat C\to\cat D$ induces a functor $\abs{F}\colon\abs{\cat C}\to\abs{\cat D}$ between underlying categories by postcomposing with the morphism part of $F$.
Concretely, a selector $\abs{f}\colon 1_\otimes\to\cat C(c,c')$ is sent to 
  \begin{equation*}
    1_\otimes\to[\abs{f}]\cat C(c,c')\to[F_{c,c'}]\cat D(Fc,Fc').
  \end{equation*}
We write $\abs{Ff}\coloneqq\abs{F}\abs{f}$ for this selector which represents an abstract morphism $Ff\colon Fc\to Fc'$ in the $\cat V$-category $\cat D$.
If $F=P$ happens to be a $\cat V$-bifunctor $P\colon\cat C\otimes\cat D\to\cat E$ we obtain an ordinary bifunctor from its underlying functor 
by precomposition with the Eilenberg--Zilber functor from the example above:
\begin{equation*}
  \abs{\cat C}\times\abs{\cat D}
    \to
  \abs{\cat C\otimes\cat D}
    \to[\abs{P}]
  \abs{\cat E}. 
\end{equation*}
We denote the image of a pair of selectors $\abs{f}$, $\abs{g}$ under this functor by $\abs{P(f,g)}$.
Fixing an object $d$ of $\cat D$ (and thus of $\abs{\cat D}$) induces a diagram of ordinary functors
\begin{equation*}
\begin{tikzcd}
  \abs{\cat C}
    \arrow[r, "\abs{P(-,d)}"]
    \arrow[dr, "\abs{(-,d)}"{description}]
    \arrow[d]
  &
  \abs{\cat E}
  \\
  \abs{\cat C}\times\abs{\cat D}
    \arrow[r]
  &
  \abs{\cat C\otimes\cat D}
    \arrow[u, "\abs{P}"']
\end{tikzcd},
\end{equation*}
where the bottom functor is the canonical one and the left functor sends an object $c$ to the pair $(c,d)$ and a selector $\abs{f}$ to the pair $(\abs{f},\abs{1_d})$.
The right triangle commutes because $\abs{-}$ behaves functorially with respect to composition of functors and one quickly checks by hand that the left triangle commutes.
This means in paricular, that $\abs{P(f,1_d)}$ in the above sense (this is the path along the bottom in the diagram) is equal to $\abs{P(f,d)}=\abs{P(-,d)}\abs{f}$.
Similarly $\abs{P(1_c,g)}=\abs{P(c,g)}$ for a fixed object $c$ of $\cat C$ and a selector $\abs{g}$ in $\abs{\cat D}$.

\begin{example}
  Let $\cat V$ be symmetric monoidal closed and $\cat C$ be a $\cat V$-category.
  Suppose we are given abstract morphisms $f\colon x'\to x$ and $g\colon y\to y'$ in $\cat C$.
  Applying the above constructions to the enriched hom-bifunctor $\cat C$ and using the isomorphism $\abs{\underline{\cat V}}\cong\cat V$ produces a morphism $\cat C(f,g)\colon\cat C(x,y)\to\cat C(x',y')$ in $\cat V$.
  In detail, this morphism is equal to
  \begin{equation*}
    \cat C(x,y)\cong 1_\otimes\otimes\cat C(x,y)\otimes 1_\otimes
      \to[\abs{f}\otimes 1\otimes\abs{g}]
    \cat C(y,y')\otimes\cat C(x,y)\otimes\cat C(x',x)
      \to
    \cat C(x',y'),
  \end{equation*}
where the last morphism is given by repeated composition (the order of composition is irrelevant by associativity).
The above considerations show that $\cat C(1_x,g)$ is equal to $\cat C(x,g)$, the image of $g$ under the underlying functor of the covariant enriched hom-functor, and acts by postcomposition with $g$.
Similarly $\cat C(f,1_y)=\cat C(f,y)$ acts by precomposition with $f$.
\end{example}
\begin{remark}
  The underlying functor $\abs{\cat C}\colon\abs{\cat C\op\otimes\cat C}\to\abs{\cat V}$ of the enriched hom-bifunctor is \emph{not} the set-valued hom-bifunctor of the underlying category $\abs{\cat C}$ of $\cat C$, so this notation is ambiguous!
  However, in case $\cat C=\underline{\cat V}$, the underlying functor of the enriched hom-bifunctor precomposed with the Eilenberg--Zilber functor corresponds to the internal hom under the identification $\abs{\underline{\cat V}}=\cat V$, all under the condition that  $\cat V$ is symmetric monoidal closed, of course.
\end{remark}










\subsection{Enriched natural transformations}
The underlying functors of the enriched hom-functors allow us to give a simple definition of enriched natural transformations.
Let $\cat V$ be a symmetric monoidal category.
A \emph{$\cat V$-natural transformation} $\alpha\colon F\to G$ between two $\cat V$-functors $F,G\colon\cat C\to\cat D$ consists of morphisms $\alpha_c\colon Fc\to Gc$ in $\cat D$ for each object $c$ of $\cat C$, specified by selectors $\abs{\alpha_c}\colon 1_\otimes\to\cat D(Fc,Gc)$, such that for all objects $c,c'$ of $\cat C$ the following diagram commutes:
\begin{equation}\label{diagram:naturality}
\begin{tikzcd}
  &
  {\cat D(Fc,Fc')}
    \arrow[rd, "{\cat D(Fc,\alpha_{c'})}"]
  & \\
  {\cat C(c,c')}
    \arrow[ru, "F_{c,c'}"]
    \arrow[rd, "G_{c,c'}"'] 
  & &
  {\cat D(Fc,Gc')}
  \\
  &
  {\cat D(Gc,Gc')}
    \arrow[ru, "{\cat D(\alpha_c, Gc')}"'] 
  &
\end{tikzcd}
\end{equation}
By definition of post- and precomposition, this is equivalent to the commutativity of the following diagram, which does not use the enriched represented functors explicitly.
\begin{equation*}
\begin{tikzcd}[column sep={4.5cm,between origins}]
  &[-3cm]
  1_\otimes\otimes\cat C(c,c')
  \arrow[r, "{\alpha_{c'}\otimes F_{c,c'}}"]
  &
  \cat D(Fc',Gc')\otimes\cat D(Fc,Fc')
  \arrow[rd, "\circ"]
  &[-3cm]
  \\
  \cat C(c,c')
  \arrow[ru, "\lambda^{-1}"]
  \arrow[rd, "\rho^{-1}"']
  &&&
  \cat D(Fc,Gc')
  \\
  &
  \cat C(c,c')\otimes 1_\otimes
  \arrow[r, "{G_{c,c'}\otimes\alpha_c}"']
  &
  \cat D(Gc,Gc')\otimes \cat D(Fc, Gc)
  \arrow[ru, "\circ"']
  &
\end{tikzcd}
\end{equation*}

Given $\cat V$-natural transformations $F\to[\alpha]G\to[\beta]H$ between $\cat V$-functors from $\cat C$ to $\cat D$, there is a \emph{vertical composition} $\beta\cdot\alpha\colon F\to H$, which is defined by the selectors
\begin{equation*}
  1_\otimes
    \to[\abs{\beta_c}\otimes\abs{\alpha_c}]
  \cat D(Gc,Hc)\otimes\cat D(Fc,Gc)
    \to[\circ]
  \cat D(Fc,Hc).
\end{equation*}
Its $\cat V$-naturality is exhibited by the diagram
\begin{equation*}
\begin{tikzcd}[column sep=small]
  {\cat C(c,c')} \arrow[r] \arrow[d] \arrow[rd] & {\cat D(Fc,Fc')} \arrow[rd]          &                            \\
  {\cat D(Hc,Hc')} \arrow[rd]                   & {\cat D(Gc,Gc')} \arrow[r] \arrow[d] & {\cat D(Fc,Gc')} \arrow[d] \\
                                              & {\cat D(Gc,Hc')} \arrow[r]           & {\cat D(Fc,Hc')}          
\end{tikzcd},
\end{equation*}
where the square at the bottom right commutes by associativity of composition and the other two squares commute by $\cat V$-naturality.

Now consider the following situation of $\cat V$-categories, $\cat V$-functors and $\cat V$-natural transformations:
\begin{equation*}
\begin{tikzcd}[column sep=large]
  \cat C
    \arrow[r, bend left=40, "F"{name=F}]
    \arrow[r, bend right=40, "G"'{name=G}]
    \arrow[rightarrow,from=F,to=G, "\alpha",shorten=1ex]
  & 
  \cat D
    \arrow[r, bend left=40, "H"{name=H}]
    \arrow[r, bend right=40, "K"'{name=K}]
    \arrow[rightarrow,from=H,to=K, "\beta",shorten=1ex]
  &
  \cat E
\end{tikzcd}.
\end{equation*}
Define a \emph{horizontal composition} $\beta\ast\alpha\colon HF\to KG$ of $\alpha$ and $\beta$ by either of the two paths in the diagram 
\begin{equation*}
\begin{tikzcd}
                                      &                                       & {\cat E(HFc,HGc)} \arrow[rd] &                   \\
1_\otimes \arrow[r, "\abs{\alpha_c}"] & {\cat D(Fc,Gc)} \arrow[ru] \arrow[rd] &                              & {\cat E(HFc,KGc)} \\
                                      &                                       & {\cat E(KFc,KGc)} \arrow[ru] &                  
\end{tikzcd},
\end{equation*}
whose right part is a $\cat V$-naturality square for $\beta$.
This defines a $\cat V$-natural transformation by the commutativity of the diagram
\begin{equation*}
\begin{tikzcd}
  {\cat C(c,c')} \arrow[r] \arrow[d]   & {\cat D(Fc,Fc')} \arrow[r] \arrow[d] & {\cat E(HFc,HFc')} \arrow[d] \\
  {\cat D(Gc,Gc')} \arrow[d] \arrow[r] & {\cat D(Fc,Gc')} \arrow[d] \arrow[r] & {\cat E(HFc,HGc')} \arrow[d] \\
  {\cat E(KGc,KGc')} \arrow[r]         & {\cat E(KFc, KGc')} \arrow[r]        & {\cat E(HFc,KGc')}          
\end{tikzcd},
\end{equation*}
which consists of two $\cat V$-naturality squares, one for $\alpha$ in the top left and one for $\beta$ in the bottom right, and two squares which commute by functoriality of $H$, respectively $K$.

\begin{remark}
  Any $\cat V$-natural transformation $\alpha$ between two $\cat V$-functors induces an ordinary natural transformation $\abs{\alpha}$ between the corresponding underlying functors, the \emph{underlying natural transformation} of $\alpha$.
  Its components are simply the selectors $\abs{\alpha_c}$ of the components of $\alpha$ and naturality follows by applying the functor $\cat V(1_\otimes,-)$ to the naturality diagram \eqref{diagram:naturality}.
  Note that, by definition, two $\cat V$-natural transformations are equal if and only if their underlying natural transformations are.
\end{remark}





\subsection{Isomorphisms}
A morphism $f\colon x\to y$ in a $\cat V$-category $\cat C$ is an \emph{isomorphism} if there exists an \emph{inverse} morphism $g\colon y\to x$ with $f\circ g=1_y$ and $g\circ f=1_x$, meaning that the morphisms
\begin{gather*}
  1_\otimes\cong 1_\otimes\otimes 1_\otimes\to[\abs{f}\otimes\abs{g}]\cat C(x,y)\otimes\cat C(y,x)\to[\circ]\cat C(y,y)\\
  1_\otimes\cong 1_\otimes\otimes 1_\otimes\to[\abs{f}\otimes\abs{g}]\cat C(y,x)\otimes\cat C(x,y)\to[\circ]\cat C(x,x)
\end{gather*}
each select the corresponding identity arrows.
By definition a morphism is an isomorphism if and only if its underlying selector is an isomorphism in $\abs{\cat C}$.
This does \emph{not} mean that the corresponding morphism $1_\otimes\to\cat C(x,y)$ in $\cat V$ is an isomorphism!

A $\cat V$-natural transformation $\alpha$ between two $\cat V$-functors $F,G\colon\cat C\to\cat D$ is called a \emph{$\cat V$-natural isomorphism} if all of its components are isomorphisms in the above sense.
One can check directly that inverses of the components assemble to an inverse $\cat V$-natural transformation under vertical composition.
Again, by definition, a $\cat V$-natural transformation is a $\cat V$-natural isomorphism if and only if its underlying natural transformation between the corresponding underlying functors is a natural isomorphism.

\begin{remark}
Later on we will see, that $\cat V$-natural isomorphisms are really just isomorphisms in a $\cat V$-category whose objects are $\cat V$-functors and whose morphisms are given by the $\cat V$-natural transformations (at least if a certain size condition is satisfied).
\end{remark}
