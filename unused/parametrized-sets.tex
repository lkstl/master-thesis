\section{Sheaves as spaces}
Recall Grothendieck's functorial approach to algebraic geometry.
Instead of describing a scheme $X$ as a locally ringed space, it is identified with its functor of points, a presheaf on the category of affine schemes, sending an affine scheme $\Spec A$ to the set $X(\Spec A)=X(A)$ of maps from $\Spec A$ to $X$, the $A$-valued points of $X$.
Starting from this perspective, the category of affine schemes is simply defined to be dual to the category of commutative rings, with $\Spec$ the duality functor.
More generally, one can, for any commutative ring $A$, consider affine $A$-schemes, which are formal duals of commutative $A$-algebras.
\todo{Size issues footnote?}
From this point of view, a scheme is not conceived as a set equipped with some structure but as a presheaf $X$ -- a parametrized set, where the parameters are affine schemes and their interrelations, expressed by morphisms of affine schemes, are respected by functoriality of $X$.
Consequently, the right notion of morphism between parametrized sets are natural transformations $X\to Y$ which map each parametrization of $X$ over an affine scheme $\Spec A$ to a parametrization of $Y$ over $\Spec A$ and this mapping is consistent with respect to change of parameter space $\Spec A\to \Spec B$ as exhibited by naturality.

Generalizing this perspective, we view the objects of a small category $\cat C$ as affine spaces and the category of presheaves $\cocompletion{\cat C}$ over $\cat C$ as a category of spaces modeled on such affine spaces.
Any object $c$ of $\cat C$ yields a presheaf over $\cat C$ by identifying it with its image under the Yoneda embedding $\cat C\to[\yoneda] \cocompletion{\cat C}$ sending $c$ to the representable presheaf $\yoneda(c)=\Hom{\cat C}{-}{c}$ of maps into $c$ and this way $\cat C$ becomes a full subcategory of $\cocompletion{\cat C}$.
For this reason we also interpret elements of $X(c)$ for an \emph{arbitrary} presheaf $X$ as maps from $c$ to $X$ or parametrizations of $X$ by $c$.
According to the Yoneda lemma this view is consistent with the Yoneda embedding of affine spaces as representable presheaves:
\[X(c)\cong \Hom{\cocompletion{\cat C}}{\yoneda(c)}{X}.\]
To any presheaf $X$ we may associate the comma category $\commacat{\yoneda}{X}$ whose objects are maps $\yoneda(c)\to[x] X$ and whose morphisms are given by commuting triangles
\begin{center}
\begin{tikzcd}[column sep = small]
  {\yoneda(c)}\arrow[rr, "\yoneda(f)"]\arrow[dr, "x"'] && 
  {\yoneda(c')}\arrow[dl, "x'"]\\
  &X
\end{tikzcd}.
\end{center}
That is, objects are parametrizations $x\in X(c)$ of $X$ by $c$ and morphisms are maps $c\to[f] c'$ between parameter spaces such that the parametrization $x$ is send to $x'$ by precomposition with $f$, so $X(f)(x')=x$.
This comma category of parametrizations is usually called the \emph{category of elements} of $X$.
There is a canonical projection functor from the category of elements of a presheaf to $\cat C$, sending a parametrization to the parameter space it is defined on.
Composing this functor with the Yoneda embedding yields a small diagram
\[\commacat{\yoneda}{X}\to\cat C\to[\yoneda]\cocompletion{\cat C}\]
and $X$ is obviously a cocone under this diagram.
In fact, it is the initial cocone and thus canonically a colimit of representable presheaves.
This colimit construction canonically exhibits the space $X$ as a glueing of affine spaces in $\cat C$.
One could say, a presheaf is nothing more than its own "instruction" on how to glue it from affine spaces.

\begin{example}
  Let us give a geometric example why it is desirable to be able to glue affine spaces in a larger category containing that of affine spaces.
  Namely, we give an example of a nonaffine but geometrically meaningful presheaf over the category of affine schemes.
  We suppress the Yoneda embedding, identifying an affine scheme $\Spec A$ with the presheaf it represents, so $B$-valued points in $(\Spec A)(B)$ are simply ring maps $A\to B$.
  An \emph{open subfunctor} of $\Spec A$ is a presheaf of the form 
  \[B\mapsto(\Spec A)_{\mathfrak a}\coloneqq\setbuilder*{\varphi\in(\Spec A)(B)}{\varphi(\mathfrak{a})\text{ generates the unit ideal in } B}\]
  for some ideal $\mathfrak{a}\subset A$.
  In case $A=\set Z[x,y]$, the presheaf $\set A^{2}\coloneqq\Spec A$ describes the affine plane, as its $A$-valued points are given by $2$-tuples of ring elements in $A$.
  The open subfunctor of $\set A^{2}$ defined by the ideal $(x,y)$ in $A$ is called the punctured affine plane because its $k$-valued points are given by $k^2\setminus\{(0,0)\}$, for $k$ a field.
  We obtain two more open subfunctors $\set A^{2}_x$, $\set A^{2}_y$, both affine and contained in $\set A^{2}_{(x,y)}$ (objectwise).
  Namely, $\set A^{2}_x$ and $\set A^{2}_y$ are spectra of localizations of $A$ at $x$ respectively $y$.
  Now suppose the punctured affine plane were representable by an affine scheme as well, say $\set A^{2}_{(x,y)}=\Spec B$.
  The Yoneda embedding is faithful, hence reflects monomorphisms,
  so we get epimorphisms
  \[A \epito B \epito A_x,A_y\]
  in the category of rings, factorizing the localization maps $A\to A_x$ respectively $A\to A_y$.
  These maps are injective because $x$ and $y$ are regular elements of $A$, hence $A$ maps injectively into $B$.
  From the above chains of epimorphisms also follows, that the isomorphism of $A$ with the intersection $A_x\intersection A_y$ inside the total fraction field of $A$ factorizes through an epimorphism to $B$.
  This allows us to show, that $A\to B$ is also surjective, thus an isomorphism of rings. 
  A contradiction, the punctured plane can not be isomorphic to the affine plane.  
\end{example}

Recall that limits and colimits in a category of presheaves are formed objectwise.
While this behaviour is expected for limits by the Yoneda lemma, it does spawn some nongeometric phenomena in the case of colimits.
To see this, consider two maps of affine spaces $u\monoto c$, $v\monoto c$ in $\cat C$.
We imagine these to be inclusions of open subspaces.
The intersection of $u$ and $v$ inside $c$ is given by the pullback
\begin{center}
\begin{tikzcd}[arrows=hook]
  {u\intersection v} \arrow[r] \arrow[d] & v \arrow[d]\\
  u         \arrow[r]           & c
\end{tikzcd}.
\end{center}
The Yoneda embedding preserves limits, so it does not matter whether we form intersections in $\cat C$ or in its presheaf category: $\yoneda(u\intersection v)=(\yoneda u)\intersection (\yoneda v)$.
Now suppose that $c$ is covered by $u\monoto c$ and $v\monoto c$, in that the diagram above is additionaly a pushout square.
Colimits are generally not preserved by the Yoneda embedding, so the map $(\yoneda u)\union(\yoneda v)\to \yoneda c$ induced from the universal property of the pushout
\begin{center}
\begin{tikzcd}
  {(\yoneda u)\intersection (\yoneda v)} \arrow[r, hook] \arrow[d] & {\yoneda v} \arrow[d, hook]\\
  {\yoneda u}         \arrow[r]           & {(\yoneda u)\union (\yoneda v)}
\end{tikzcd}
\end{center}
is usually not an isomorphism.
Thus, the geometrically meaningful union $c$ in our category of affine spaces is potentially replaced by a new union $(\yoneda u)\union (\yoneda v)$ in the bigger category of presheaves.
Let us showcase the downsides of both notions of unions:
\begin{enumerate}
\item Due to the fact that colimits of presheaves are computed objectwise, the set of maps from an affine space $d$ into the new union are given by
  \begin{align*}
    \left((\yoneda u)\coproduct_{\yoneda(u\intersection v)}(\yoneda v)\right)(d)
   % =(\yoneda u)(d)\coproduct_{\yoneda(u\intersection v)(d)}(\yoneda v)(d)
    =\Hom{\cat C}{d}{u}\coproduct_{\Hom{\cat C}{d}{u\intersection v}}\Hom{\cat C}{d}{v}.
  \end{align*}
  That is, maps $d\to (\yoneda u)\union(\yoneda v)$ are presented by maps $d\to u$ and $d\to v$ that are identified if they factor through $u\intersection v$.
  In particular, it is impossible for the image of a map to lie in both $u$ and $v$ without being concentrated in their intersection.
  \todo[inline]{Include example $\Spec\set Z[p^{-1}]\union\Spec\set Z[q^{-1}]$?}
\item Since $\yoneda c$ need not satisfy the universal property of the pushout $(\yoneda u)\union (\yoneda v)$, maps into a presheaf $X$ may no longer be given by a pair of maps $\yoneda u\to X$, $\yoneda v\to X$ agreeing on the intersection $(\yoneda u)\intersection(\yoneda v)$, unless $X$ is representable.
  \[X(c)\neq X(u)\product_{X(u\intersection v)} X(v)\]
  \todo[inline]{Include example (any presheaf that is not a sheaf)? Maybe I should skip the examples here?}
\end{enumerate}

We see that neither of the two notions of union behave in a satisfying way.
This can be observed for all kinds of glueing constructions, stemming from the fact that, for any diagram $F$ in $\cat C$, the canonical map
\[\colim\yoneda F\to \yoneda\colim F\]
is usually not invertible.
When we transitioned from $\cat C$ to its category of presheaves we added new colimits in a universal way%
  \footnote{The category of presheaves $\cocompletion{\cat C}$ is the \emph{free cocompletion} of $\cat C$, meaning that any colimit preserving functor starting in $\cat C$ factors uniquely through $\cocompletion{\cat C}$ in a $2$-categorical sense.},
   but destroyed existing colimits in $\cat C$.
The solution to this problem is to construct a universal category, where such morphisms which ought to be invertible, really are invertible.

\begin{definition}
Let $\cat D$ be a category and $\cat W\subset\cat D$ a subcategory.
A \emph{localization} of $\cat D$ at (the morphisms of) $\cat W$ is a category $\cat D[\cat W^{-1}]$ together with a functor $\cat D \to[L] \cat D[\cat W^{-1}]$ sending all morphisms in $\cat W$ to isomorphisms, such that the following 2-categorical universal property is satisfied:
For any functor $\cat D\to[F] \cat C$ sending all morphisms in $\cat W$ to isomorphisms, there exists a functor $\cat D[\cat W^{-1}] \to[\bar F] \cat C$ together with a natural isomorphism $F\To[\psi] \bar FL$.
This datum is unique up to a unique natural isomorphism, in that, given another functor ${\cat D[\cat W^{-1}]}\to[\bar F']{\cat C}$ and a natural isomorphism $F\To[\psi'] \bar F'L$, there is a unique natural transformation (necessarily an isomorphism) $\bar F\To[\theta] \bar F'$ such that $\theta L\cdot\psi=\psi'$.
\begin{center}
  \begin{tikzcd}[row sep = huge, column sep = large]
    \cat D \arrow[rr, "F", ""'{name=F}, ""'{name=Fstart, near start}] \arrow[d, "L"'] && \cat C\\
    {\cat D[\cat W^{-1}]} \arrow[urr, "\bar{F}'" description, ""{name=B, near start}, bend right=35, ""{name=Bmid}] 
    \arrow[Rightarrow, from=Fstart, start anchor={[xshift=-1ex]}, to=B, gray, "\psi'" near start, "\sim"'{near end, rotate=-70, scale=0.8, yshift=-0.5ex}, bend left =20]
    \arrow[urr, "\bar{F}" description, ""{name=A, near start}, bend left=10, crossing over, ""'{name=Amid}]
    \arrow[Rightarrow, from=Fstart, start anchor={[xshift=-2ex]}, to=A, gray, "\psi"' near start, "\sim"{near start, rotate=-65, scale=0.8}, bend right=10]
    \arrow[Rightarrow, from=Amid, to=Bmid, gray, "\exists !\theta"]
  \end{tikzcd}
\end{center}
\end{definition}

If the subcategory $\cat W\subset\cat D$ is a small category, it is always possible to construct a localization, see for example \cite[Proposition 5.2.2]{borceux1994handbook1}.
The resulting category is quite impractical because its morphisms consist of equivalence classes of finite paths in the underlying graph of $\cat D$.
In certain cases there are much more tractable solutions as the following example shows.

\begin{example}
  Let $\hookadj{\cat E}{L}{\iota}{\cat D}$ be an adjunction with full and faithful right adjoint, exhibiting $\cat E$ as a reflective subcategory of $\cat D$.
  The pair $(\cat E, L)$ then is a localization of $\cat D$ at the subcategory spanned by the \emph{$L$-local} morphisms, that is, those morphisms that are sent to isomorphisms by $L$:
  If $\eta$ denotes the unit of this adjunction, one can check that, given any functor $\cat D\to[F] \cat C$ sending $L$-local morphisms to isomorphisms, $\bar F\coloneqq F\iota$ is a factorization through $L$ via the natural transformation $F\To[F\eta] \bar FL$.
  In fact, $F\eta$ is a natural isomorphism because the components of the unit $\eta$ are inverted by $L$.
  Namely, $L\eta$ is inverse to $\varepsilon L$ by one of the adjunction's triangle identities and since $\iota$ is full and faithful if and only if the counit $\varepsilon$ is invertible.
  This proves existence of a factorization.
  For uniqueness up to unique isomorphism we refer to \cite[Proposition 1.77]{nlab2019categories}.
\end{example}
\begin{remark}
  Notice that only the unit components $\eta_d$ were required to be $L$-local for the proof in the above example, hence $(\cat E, L)$ is already the localization of $\cat D$ at the full subcategory spanned by the unit components of the adjunction.
  In general, any localization ${\cat D}\to[L] {\cat D[\cat W^{-1}]}$ at a subcategory $\cat W$ defines another subcategory $\bar{\cat W}$, spanned by the $L$-local morphisms, whose localization is also given by $\cat D[\cat W^{-1}]$.
  We call $\bar{\cat W}$ the \emph{saturation} of $\cat W$.
\end{remark}

A localization with full and faithful right adjoint is called a \emph{reflective localization}, so any reflective subcategory yields a reflective localization and vice versa.
Reflective localizations are much better behaved because the localized category is a full subcategory of the original category, so the morphism sets stay the same.
Moreover, the essential image of a reflective subcategory has a concrete description.

\begin{definition}
  Let $\cat W$ be a subcategory of a category $\cat D$.
  An object $d$ is called \emph{$\cat W$-local} if the precomposition map $\Hom{\cat D}{b}{d} \to[f^*] \Hom{\cat D}{a}{d}$ is a bijection for all morphisms $a\to[f] b$ in $\cat W$.
  We obtain a full subcategory $\cat D_{\cat W}$ of $\cat W$-local objects.
  Suppose that $\cat W$ is a subcategory of $L$-local morphisms for some functor $L$, then the $\cat W$-local objects are also called \emph{$L$-local}.
\end{definition}
\begin{proposition}
  If $L$ is the reflector of a reflective subcategory $\hookadj{\cat E}{L}{\iota}{\cat D}$ the essential image of the inclusion functor $\iota$ is the full subcategory of $L$-local objects.
\end{proposition}
\begin{proof}
  See \cite[Proposition 1.81]{nlab2019categories}.
\end{proof}

Now that we have established why reflective localizations are nice, it would be useful to know which subcategories $\cat W$ give rise to reflective localizations.
In other words, we would like to know when the inclusion functor of the full subcategory of $\cat W$-local objects admits a left adjoint.
Note that $\cat D_{\cat W}$ is closed under limits, meaning the inclusion $\monoto[\iota]{\cat D_{\cat W}}{\cat D}$ preserves limits.
In particular $\cat D$ is complete if this is the case for $\cat D$, so that we may apply Freyd's general adjoint functor theorem to conclude that $\iota$ has a left adjoint if and only if it satisifes the solution set condition.
There is however a much nicer adjoint functor theorem we will be able to apply.
It relies on the category $\cat D$ being nicely behaved in the following sense.

\begin{definition}
  Let $\kappa$ be a regular cardinal% 
  \footnote{An infinite cardinal $\kappa$ is \emph{regular} if the union over a set $S$ of cardinality $<\kappa$ has cardinality $<\kappa$, given that all members of $S$ have cardinality $<\kappa$.}
  and $\cat D$ be a category.
  An object $d$ of $\cat D$ is called \emph{$\kappa$-compact} if the functor $\Hom{\cat D}{d}{-}$ preserves $\kappa$-filtered colimits%
  \footnote{For a regular cardinal $\kappa$, a category $\cat I$ is \emph{$\kappa$-filtered} if each diagram with less than $\kappa$ many arrows has a cocone in $\cat I$.
  A \emph{$\kappa$-filtered colimit} is a colimit of a diagram $\cat I\to[F] \cat D$ where $\cat I$ is a $\kappa$-filtered category.}.
  The category $\cat D$ is called \emph{$\kappa$-presentable} if it is cocomplete and there exists a small full subcategory $\cat C$ of $\kappa$-compact objects such that any object of $\cat D$ is a $\kappa$-filtered colimit of objects in $\cat C$.
  We call $\cat D$ \emph{representable} if there exists a regular cardinal $\kappa$ such that $\cat D$ is $\kappa$-representable.
\end{definition}

\begin{example}
  The first regular cardinal is $\aleph_0$, the cardinality of the natural numbers.
  The category of presheaves $\cocompletion{\cat C}$ over a small category $\cat C$ is an $\aleph_0$-presentable category.
  To see this, first note that any representable presheaf on a small category $\cat C$ is $\aleph_0$-compact by the Yoneda lemma and because colimits in the category of presheaves are computed objectwise.
  Furthermore we have the following two facts:
  \begin{enumerate}
    \item Finite colimits of $\aleph_0$-compact objects are $\aleph_0$-compact.
      This follows directly from the fact that finite limits commute with $\aleph_0$-filtered colimits.
    \item The colimit of a diagram $\cat I\to[F] \cat D$ in a category with finite and $\aleph_0$-filtered colimits can be written as 
      \[\colim F=\colim_{\cat J\subset \cat I}\colim(F\vert_{\cat J}),\]
  where the first colimit runs over the ($\aleph_0$-filtered) partial order of finite full subcategories of $\cat I$.
  \todo{Stimmt das wirklich?}
  \end{enumerate}
  (There is an obvious generalization of these two facts to arbitrary regular cardinals.)
  We already saw that any presheaf $X$ is a colimit of representable presheaves and the above considerations imply that $X$ is an $\aleph_0$-filtered colimit of $\aleph_0$-compact objects.
  Since $\cocompletion{\cat C}$ in addition is cocomplete, it is $\aleph_0$-presentable.
\end{example}

As it turns out, any $\kappa$-presentable category $\cat D$ is a reflective localization of a presheaf category $\cocompletion{\cat C}$, where $\cat C$ can be chosen as the full subcategory of all $\kappa$-compact objects in $\cat D$ (or rather, an equivalent small category), see \cite[Theorem 1.46]{adamek1994locally}.
In particular, presentable categories are not only cocomplete but also complete as the embedding of a reflective subcategory creates limits.
Since the category of presheaves over $\cat C$ is freely generated by $\cat C$ under colimits we may think of the presentable category $\cat D$ as being presented by generators from $\cat C$ and relations imposed by the reflective localization (at the $L$-local objects, if $L$ is the reflection).

\begin{theorem}
  A functor between presentable categories is right adjoint if and only if it preserves limits and $\kappa$-filtered colimits for some regular cardinal $\kappa$.
\end{theorem}
\begin{proof}
  See \cite[1.66]{adamek1994locally}.
\end{proof}

\begin{corollary}
  A full subcategory of a presentable category is reflective if it is closed under limits and $\kappa$-filtered colimits for some regular cardinal $\kappa$.
\end{corollary}
\begin{corollary}
  Let $\cat W$ be a small subcategory of a presentable category $\cat D$.
  The full subcategory $\cat D_{\cat W}$ of $\cat W$-local objects is reflective in $\cat D$.
\end{corollary}
\begin{proof}
  Let us assume that $\cat D$ is $\lambda$-presentable for some regular cardinal $\lambda$.
  Every object $a\in\cat W$ is a small colimit of $\lambda$-compact objects.
  Since $\cat W$ is small, we may choose a regular cardinal $\kappa>\lambda$, such that each of these colimits is $\kappa$-small.
  \todo{Why?}
  Any $\lambda$-compact object is also $\kappa$-compact, so each object $a\in\cat W$ is a $\kappa$-small colimit of $\kappa$-compact objects and thus itself $\kappa$-compact (since $\kappa$-small limits commute with $\kappa$-filtered colimits in the category of sets).
  Now it is easy to see that any $\kappa$-filtered colimit of $\cat W$-local objects is $\cat W$-local, so $\cat D_{\cat W}$ is closed under $\kappa$-filtered colimits.
  Also $\cat D_{\cat W}$ is obviously closed under limits and thus a reflective subcategory by the previous corollary. 
\end{proof}

Coming back to our original situation of a category of presheaves over affine spaces, we are now free to localize at any small subcategory $\cat W$ to obtain a reflective subcategory of $\cat W$-local objects, which should be better suited for the geometric situation we intend to model.
But how to choose $\cat W$?
Previously we wanted to invert morphisms of the form $(\yoneda u)\union(\yoneda v)\to \yoneda c$, where $c=u\union v$, so that the Yoneda embedding preserves the union.
This was because we thought of $(u\monoto c,v\monoto c)$ as an open covering of $c$, thus encoding some topological or geometric information which we deemed worthy of preservation.
In general, we define a \emph{covering family} of an affine space $c$ to be a family of morphisms $\setbuilder{u_i\to c}{i\in I}$ with common codomain $c$.
Neither do the morphisms have to be monic nor does $c$ need to be the union of the $u_i$, which gives us more flexibility as to what should count as a cover.
\todo{Examples?}

Any covering family $\setbuilder{u_i\to c}{i\in I}$ induces a morphism from $U\coloneqq\Coproduct_i \yoneda u_i$ to $\yoneda c$ in $\cat C$.
Forming the pullback $U\product_{\yoneda c}U$ of this morphism along itself induces a diagram $U\product_{\yoneda c} U\rightrightarrows U$, its \emph{$1$-trunctated \v{C}ech nerve}, denoted by $\Cech(U)$.
Its colimit admits a unique morphism
  \[\colim\Cech(U)\to \yoneda c.\]
In the category of presheaves coproducts are stable under pullback because this is the case for sets.
Hence $U\product_{\yoneda c}U$ is isomorphic to $\Coproduct_{i,j}\yoneda u_i\product_{\yoneda c}\yoneda u_j$, whose cofactors in turn are isomorphic to $\yoneda (u_i\product_c u_j)$ if $\cat C$ admits these intersections.
By thinking of $U$ as a covering of $c$, we should therefore impose the relation that the morphism $\colim\Cech(U)\to \yoneda c$ is an isomorphism.
