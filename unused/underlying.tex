\subsection{Completeness}
Any enriched universal property specified by a $\cat V$-functor $F\colon\cat C\to\underline{\cat V}$ induces an ordinary universal property on its underlying category, specified by
\begin{equation*}
  \abs{\cat C}
    \to[\abs{F}]
  \abs{\underline{\cat V}}
    \cong
  \cat V
    \to[\cat V(1_\otimes,-)]
  \mathrm{Set},
\end{equation*}
where $\cat V(1_\otimes,-)$ sends an object to its underlying elements.
We write $\cat V(1_\otimes, F)$ for this functor and call the induced universal property on $\abs{\cat V}$ the \emph{underlying universal property} of $F$.

\begin{example}
  For $\cat C$ the enriched hom-bifunctor of the $\cat V$-category $\cat C$, the functor $\cat V(1_\otimes,\cat C)$, precomposed with the Eilenberg-Zilber functor $\abs{\cat C}\op\times\abs{\cat C}\to\abs{\cat C\op\otimes\cat C}$, is the ordinary hom-bifunctor on $\cat C$.
  In particular, the ordinary covariant and contravariant hom-functors on $\abs{\cat C}$ are given by $\cat V(1_\otimes,\cat C(c,-))$, respectively $\cat V(1_\otimes,\cat C(-,c))$.
\end{example}

Obviously, if two $\cat V$-functors are $\cat V$-naturally isomorphic, their underlying functors are naturally isomorphic.
Thus, in light of the example, if an object $c$ of $\cat C$ satisfies the enriched universal property specified by $F$, the same object, now understood as an object of $\abs{\cat C}$, satisfies the underlying universal property specified by $\cat V(1_\otimes,F)$.

The converse will be not true, in general.
However, if $c$ is a representing object for $\cat V(1_\otimes,F)$, the universal element of this representation is in fact an element of $Fc$ in the enriched sense - it is nothing but a map $1_\otimes\to Fc$.
By the weak Yoneda lemma, this induces a $\cat V$-natural transformation $\alpha$ from $\cat C(c,-)$ to $F$.
To check if this is an isomorphism it suffices to check whether it induces an isomorphism (in $\cat V$) on components
\begin{equation*}
  \alpha_d\colon\cat C(c,d)\to Fd.
\end{equation*}
We know that it induces an isomorphism on elements, that is, applying $\cat V(1_\otimes,-)$ yields an isomorphism.
But in general, this does not suffice for the component $\alpha_d$ to be an isomorphism.
By the ordinary Yoneda lemma, however, it suffices that \emph{all} functors $\cat V(v,-)$ send $\alpha_d$ to an isomorphism.
This means that $c$ needs to satisfy the universal property on all \emph{$v$-elements} to satisfy the enriched universal property.

Now suppose that $\cat C$ is powered over $\cat V$, which means that there is an isomorphism
\begin{equation*}
  \cat C(c,d)^v\cong\cat C(c,d^v),
\end{equation*}
which is $\cat V$-natural in all variables.
Of course this also holds naturally on the underlying functors.
In the situation above, this leads us to considering the following diagram:
\begin{equation*}
\begin{tikzcd}[column sep=large]
  \cat V(v,\cat C(c,d))
    \arrow[r,"{\cat V(v,\alpha_c)}"]
    \arrow[d,"\sim"'{rotate=90,xshift=7pt,yshift=3pt}]
  &
  \cat V(v,Fe)
    \arrow[d,"\sim"'{rotate=90,xshift=7pt,yshift=3pt}]
  \\
  \cat V(1_\otimes,\cat D(d,e)^v)
    \arrow[r,"{\cat V(1_\otimes,\alpha_c^v)}"]
    \arrow[d,"\sim"'{rotate=90,xshift=7pt,yshift=3pt}]
  &
  \cat V(1_\otimes,(Fe)^v)
    \arrow[d,dotted,"\sim"'{rotate=90,xshift=7pt,yshift=3pt}]
  \\
  \cat V(1_\otimes,\cat D(d,e^v))
    \arrow[r,"{\cat V(1_\otimes,\alpha_{c^v})}","\sim"']
  &
  \cat V(1_\otimes,F(e^v))
\end{tikzcd}.
\end{equation*}
Note that the bottom arrow is an isomorphism because $d$ represents $F$ on elements.
If the dotted isomorphism exists, this shows that $d$ already satisfies the enriched universal property induced by $F$, not only its underlying one.
There is an obvious dual version of this result for copowered $\cat V$-categories and contravariant $\cat V$-functors.
