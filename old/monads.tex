\section{Monads and adjunctions}

\begin{definition}
  A \emph{monad} on a category $\cat D$ is a triple $T=(T,\mu,\eta)$ consisting of an endofunctor $\cat D\xrightarrow{T}\cat D$ and two natural transformations, the \emph{multiplication} $T^2\xrightarrow{\mu} T$ and the \emph{unit} $1\xrightarrow{\eta} T$, such that the following diagrams commute:
  \begin{center}
    \begin{tikzcd}
      T^3\arrow[r, "T\mu"]\arrow[d, "\mu T"'] & T^2\arrow[d, "\mu"] && T\arrow[r, "T\eta"]\arrow[dr, "1"'] & T^2\arrow[d, "\mu"] & T\arrow[l, "\eta T"']\arrow[dl, "1"]\\
      T^2\arrow[r, "\mu"] & T   &&   & T   &
    \end{tikzcd}.
  \end{center}

  Dually, a \emph{comonad} on $\cat D$ is a triple $G=(G,\delta,\epsilon)$ consisting of an endofunctor $\cat D\xrightarrow{G}\cat D$ and two natural transformations, the \emph{comultiplication} $G\xrightarrow{\delta} G^2$ and the \emph{counit} $G\xrightarrow{\epsilon}1$, such that the following diagrams commute:
  \begin{center}
    \begin{tikzcd}
      G\arrow[r, "\delta"]\arrow[d, "\delta"']   & G^2\arrow[d, "G\delta"] &&   & G\arrow[dl, "1"']\arrow[dr, "1"]\arrow[d, "\delta"]   &\\
      G^2\arrow[r, "\delta G"] & G^3 && G\arrow[r, "G\epsilon"'] & G^2 & G\arrow[l, "\epsilon G"]
    \end{tikzcd}.
  \end{center}
\end{definition}


\begin{remark}\label{remark:dual_comonad}
  A (co)monad is nothing but a (co)monoid object in the monoidal category $([\cat D,\cat D],\circ, 1)$ of endofunctors of $\cat D$.
  Since a comonoid in a monoidal category is simply a monoid in its dual category and $[D,D]\op$ is equivalent to $[D\op,D\op]$, we may view a comonad on $\cat D$ as a monad on $\cat D\op$.
\end{remark}


\begin{example}
  The prime example of a (co)monad is the one given by an adjunction.
  In some sense it is the only example as we will see below.
  Let $\adjunction{\cat E}{F}{U}{\cat D}$ be an adjunction with unit $1\xrightarrow{\eta} UF$ and counit $FU\xrightarrow{\epsilon} 1$.
  Then the composition $UF$ becomes a monad on $\cat D$ with unit $\eta$ and multiplication $U\epsilon F$.
  Dually $FU$ is a comonad on $\cat E$ with counit $\epsilon$ and comultiplication $F\eta U$.
\end{example}


For a given (co)monad $T$ on a category $\cat D$ we want to know whether it arises from an adjunction as in the above example.
To this end we define the category $\Adj(T)$ of solutions to this problem, namely objects of $\Adj(T)$ are adjunctions $\adjunction{\cat E}{F}{U}{\cat D}$ with specified units and counits, such that the induced (co)monad is equal to $T$.
We now construct two objects of this category showing that it is not empty and that indeed every (co)monad arises from an adjunction.


\begin{definition}
  Let $(T,\mu,\eta)$ be a monad on $\cat D$.
  A \emph{$T$-algebra} is an an object $a$ of $\cat D$ together with a morphism $Ta\xrightarrow{\alpha} a$ such that the following diagrams commute:
  \begin{center}
    \begin{tikzcd}
      T^2a\arrow[r, "T\alpha"]\arrow[d, "\mu_a"'] & Ta\arrow[d, "\alpha"] && a\arrow[r, "\eta_a"]\arrow[dr, "1"'] & Ta\arrow[d, "\alpha"]\\
      Ta\arrow[r, "\alpha"] & a &&& a
    \end{tikzcd}.
  \end{center}
  A \emph{morphism of $T$-algebras} $(a,\alpha)\to(a',\alpha')$ is given by a morphism $a\xrightarrow{f} a'$ in $\cat D$ which is compatible with the structure maps $\alpha$, $\alpha'$ in that the diagram
  \begin{center}
    \begin{tikzcd}
      Ta \arrow[r, "Tf"] \arrow[d, "\alpha"'] & Ta' \arrow[d, "\alpha'"]\\
      a \arrow[r, "f"] & a'
    \end{tikzcd}
  \end{center}
  commutes.
  Evidently this datum assembles to a category of $T$-algebras.
  We call this the \emph{Eilenberg--Moore category} of $T$ and denote it by $\EM(T)$ or $\cat D^T$.
  It comes with a forgetful functor $\EM(T)\xrightarrow{U^T}\cat D$, sending a $T$-algebra $(a,\alpha)$ to $a$.
  Any object $d$ of $\cat D$ gives rise to an algebra $(Td, \mu_d)$, the \emph{free algebra over $d$} for $T$, in a functorial way, yielding a functor $\cat D\xrightarrow{F^T}\EM(T)$ in the converse direction.

  Dually, a \emph{$G$-coalgebra} over a comonad $(G,\delta,\epsilon)$ is an object $c$ of $\cat D$ together with a morphism $c\xrightarrow{\gamma} Gc$ such that the following diagrams commute:
  \begin{center}
    \begin{tikzcd}
      c \arrow[r, "\gamma"]\arrow[d, "\gamma"'] & Gc \arrow[d, "G\gamma"] && c \arrow[d, "\gamma"'] \arrow[dr, "1"]\\
      Gc \arrow[r, "\delta_c"] & G^2c && Gc\arrow[r, "\epsilon_c"'] & c
    \end{tikzcd}.
  \end{center}
  A \emph{morphism of $G$-coalgebras} $(c,\gamma)\to(c',\gamma')$ is given by a morphism $c\xrightarrow{f} c'$ in $\cat D$ which is compatible with the structure maps $\gamma$, $\gamma'$ in $\cat D$ in that the diagram
  \begin{center}
    \begin{tikzcd}
      c \arrow[r, "f"] \arrow[d, "\gamma"'] & c' \arrow[d, "\gamma'"]\\
      Gc \arrow[r, "Gf"] & Gc'
    \end{tikzcd}
  \end{center}
  commutes.
  The resulting Eilenberg--Moore category of $G$-coalgebras is also denoted by $\EM(G)$, together with its forgetful functor $\EM(G)\xrightarrow{U^G}\cat D$.
  Again any object $d$ of $D$ yields a \emph{cofree coalgebra} $(Gd,\delta_d)$, inducing a functor $\cat D\xrightarrow{F^T} \EM(G)$. 
\end{definition}


\begin{definition}
  For any monad $T$ on a category $\cat D$ we define the \emph{Kleisli category} $\Kl(T)$ of this monad as follows:
  The objects of $\Kl(T)$ are just the objects of $\cat D$ and morphisms $d\to d'$ in $\Kl(T)$ are exactly the morphisms $d\to Td'$ in $\cat D$.
  Composition of morphisms $d'\xrightarrow{f} d''$, $d\xrightarrow{g} d'$ in $\Kl(T)$ is given by the composition 
  \[d\xrightarrow{g} Td'\xrightarrow{Tf} T^2d''\xrightarrow{\mu_{d''}}Td''\]
  in $\cat D$. 
  %\begin{center}
  %  \begin{tikzcd}[column sep=tiny, row sep=scriptsize]
  %    \Kl(T) \arrow[rr, "U_T"] && \cat D &\hspace{2cm}& \cat D \arrow[rr, "F_T"] && \Kl(T)\\
  %    d\arrow[dd, "f"'] && Td \arrow[d, "Tf"] && d\arrow[dd, "f"'] && d \arrow[d, "\eta_d"]\\
  %    & \mapsto\hspace{1.5ex} & T^2d' \arrow[d, "\mu_{d'}"] && & \hspace{1.5ex}\mapsto & Td \arrow[d, "Tf"]\\
  %    Td' && Td' && d' && Td'
  %  \end{tikzcd}
  %\end{center}
  There is a functor $\Kl(G)\xrightarrow{U_T}\cat D$, sending $d\xrightarrow{f} d'$ to the composition $Td\xrightarrow{Tf}T^2d'\xrightarrow{\mu_d'}Td'$, and a functor $D\xrightarrow{F_T} \Kl(T)$ in the other direction, sending $d\xrightarrow{f} d'$ to $d\xrightarrow{\eta_d} Td\xrightarrow{Tf} Td'$.

  We define the Kleisli category $\Kl(G)$ of a comonad $G$ on $\cat D$ in a dual fashion, where morphisms $d\to d'$ in $\Kl(G)$ are given by morphisms $Gd\to d'$ in $\cat D$.
  We also have functors $\Kl(G)\xrightarrow{U_G}\cat D$ and $\cat D\xrightarrow{F_G}\Kl(G)$, defined dually to the corresponding functors for a monad.
\end{definition}


\begin{remark}
  Using remark \ref{remark:dual_comonad} a coalgebra over a comonad $G$ is formally dual to an algebra over the dual monad $G\op$ induced by $G$.
  It follows, that $\EM(G)$ is equivalent to $\EM(G\op)\op$.
  Similarly we see that $\Kl(G)$ is equivalent to $\Kl(G\op)\op$.
\end{remark}


\begin{proposition}
  Let $(T,\mu,\eta)$ be a (co)monad on a category $\cat D$.
  The functors $\lradj{\EM(T)}{F^T}{U^T}{\cat{D}}$ and $\lradj{\Kl(T)}{F_T}{U_T}{\cat{D}}$ are adjoint and their induced (co)monad is equal to $T$.
  %The analogous result holds if $(T,\mu,\eta)$ is a comonad.
  In particular, the Eilenberg--Moore category and the Kleisli category are objects of $\Adj(T)$, the first being a terminal object, the latter an initial one.
  The unique functor $\Kl(T)\to\EM(T)$ is fully faithful and its essential image is given by the full subcategory of (co)free (co)algebras. 
\end{proposition}
\begin{proof}
  \todo[inline]{Comparison functors and reference.}
\end{proof}


Before we summarize some useful properties of the categories and adjunctions we constructed, let us recall some standard terminology from category theory concerning the interaction of limits with functors.


\begin{definition}
  Let $\cat J\xrightarrow{K}\cat D$ and $\cat D\xrightarrow{F}\cat E$ be functors.
  We say that:
  \begin{enumerate}
    \item $F$ \emph{preserves} limits of $K$ if for all limit cones $d\xrightarrow{\lambda} K$ in $\cat D$ the image cone $Fd\xrightarrow{F\lambda} FK$ is a limit cone for $FK$ in $\cat E$.
    \item $F$ \emph{reflects} limits of $K$ if each cone $d\xrightarrow{\lambda} K$ in $\cat D$, whose image cone $Fc\xrightarrow{F\lambda} FK$ is a limit cone for $FK$ in $\cat E$, is a limit cone for $K$.
    \item $F$ \emph{lifts} limits of $K$ if for each limit cone $e\xrightarrow{\theta} FK$ of $FK$ in $\cat E$ there exists a limit cone $d\xrightarrow{\lambda} K$ of $K$ in $\cat D$ which is preserved by $F$.
      In this case $Fd\to FK$ and $e\to FK$ are necessarily isomorphic cones over $FK$.
    \item $F$ \emph{creates} limits of $K$ if it lifts and reflects them.
  \end{enumerate}
\end{definition}

\begin{proposition}
  Let $(T,\mu,\eta)$ be a monad (comonad) on a category $\cat D$.
  \begin{enumerate}
    \item The forgetful functors $U^T$ and $U_T$ from the Eilenberg-Moore category, respectively the Kleisli category, are faithful.
    \item The forgetful functor $U^T$ creates all limits (colimits).
    \item The forgetful functor $U^T$ creates all colimits (limits) that are preserved by $T$ and $T^2$.
    \item Every algebra $(a,\alpha)$ (coalgebra $(c,\gamma)$) in $\EM(T)$ is a coequalizer (equalizer) of free algebras (cofree coalgebras).
      More precisely the diagram $\coequalizer{T^2a}{T\alpha}{\mu_a}{Ta}{\alpha}{a}$ (respectively $\equalizer{c}{\gamma}{Tc}{T\gamma}{\mu_c}{T^2c}$) is a split coequalizer%
      \footnote{
  A diagram $\coequalizer{a}{h}{b}{f}{g}{c}$ is a \emph{split coequalizer} if $hf=hg$ and there are maps $c\xrightarrow{s} b$ and $b\xrightarrow{t} a$ such that $hs=1_c$, $gt=1_b$ and $ft=sh$.
  It is easy to see that a split coequalizer is a coequalizer of $f$ and $g$.
  There is an obvious dual notion of a \emph{cosplit equalizer}.}
(cosplit equalizer) in $\cat D$, which implies that $\coequalizer{F^TTa}{T\alpha}{\mu_a}{F^Ta}{\alpha}{(a,\alpha)}$ is a coequalizer (respectively that \equalizer{(c,\gamma)}{\gamma}{F^Tc}{T\gamma}{\mu_c}{F^TTc} is an equalizer) by the point above.
  \end{enumerate}
\end{proposition}
\begin{proof}
  \todo[inline]{References.}
\end{proof}

These properties hold for any adjunction which is equivalent to the Eilenberg-Moore category of its induced monad or comonad.
The forgetful functor of such an adjunction is called a \emph{(co)monadic} functor.
Such functors are characterized by the following theorem.
\begin{theorem}[Beck]\label{theorem:Beck}
  Let $\cat E\xrightarrow{U}\cat D$ be a right (left) adjoint functor.
  Then $U$ is (co)monadic if and only if $U$ creates coequalizers (equalizers) of $U$-(co)split pairs%
  \footnote{If $\cat E\xrightarrow{U}\cat D$ is a functor, two parallel morphisms are called a \emph{$U$-(co)split pair} if their image fits into a split coequalizer (cosplit equalizer) diagram in $\cat D$.}
  of morphisms.
\end{theorem}
\begin{proof}
  \todo[inline]{References.}
\end{proof}


