\chapter{Higher derived geometry}
  In this chapter we introduce a notion of geometry which is suitable to describe the various kinds of spaces occuring in gauge field theory in a uniform way.
  \todo[inline]{Quick overview and references.}


  \section{Motivation}
  Our approach to geometry is based on Grothendieck's functorial or parametrized algebraic geometry.
  There, rather than viewing a scheme $X$ as a locally ringed topological space, we identify it with its functor of points
  \[A\longmapsto\Hom(\Spec A,X)\]
  from the category of commutative rings to the category of sets.
  From this point of view every scheme is a presheaf on the dual of the category of commutative rings and can therefore be regarded as an object \emph{parametrized} by commutative rings and their morphisms.
  To capture a notion of locality one may then introduce a topology on this category, which specifies those morphisms that are to be regarded as parts of open coverings, e.g. the inclusions of localization spectra $\Spec A[f^{-1}]\to\Spec A$ in the Zariski topology.
  All schemes are local in the Zariski topology, that is, a compatible family of scheme morphisms with target $X$, defined on a Zariski open cover $(\Spec A_i\to\Spec A)_{i\in I}$, can be uniquely glued to a scheme morphism $\Spec A\to X$.
  In other words, any scheme defines a sheaf on the dual category of commutative rings equipped with the Zariski topology.
  In fact, the category of schemes can be defined as the full subcategory on those Zariski sheaves that admit an open cover by representable sheaves (which are just the affine schemes, i.e. formal duals of commutative rings).
  From a categorical standpoint however, the Zariski sheaves are better behaved than schemes:
  Their collection forms a Grothendieck topos.

  \begin{definition}
    A left exact%
    \footnote{A functor is called \emph{left exact} if it preserves finite limits.}
    idempotent monad on a category is called a \emph{local operator}.
    A \emph{Grothendieck topos}%
    \footnote{There is also the more general notion of an elementary topos, but we will not use this concept.
    Thus we will just speak of "toposes" from now on, dropping the reference to Grothendieck.}
    is a category $\cat H$ which is equivalent to the category of algebras for a local operator $\modality$ on a category of presheaves over a small category $\cat C$.
    We call the pair $(\cat C,\modality)$ a \emph{site of definition} for $\cat H$.
  \end{definition}

  \begin{proposition}\label{prop:local_operator_subcategory}
    Let $\modality$ be a local operator on a category $\cat D$.
    A morphism in $\cat D$ is called \emph{$\modality$-local} if its image under $\modality$ is an isomorphism and an object $d$ in $\cat D$ is called \emph{$\modality$-local} if for all $\modality$-local morphisms $a\xrightarrow{f} b$ the induced map $\cat D(b,d)\xrightarrow{f\pull} \cat D(a,d)$ is a bijection.

    Then the category of algebras $\cat D^\modality$ for the local operator $\modality$ is a left exact reflective localization of $\cat D$ at the $\modality$-local morphisms, whose essential image in $\cat D$ is given by the full subcategory of $\modality$-local objects in $\cat D$.
    Conversely, any left exact reflective localization of $\cat D$ induces a local operator on $\cat D$ whose category of algebras is equivalent to the given localization.
    Thus (Grothendieck) toposes are, up to equivalence, precisely the left exact reflective localizations of presheaf categories over small categories, whose objects are local presheaves for the site.
  \end{proposition}
  \begin{proof}
    Suppose first that $\modality$ is an arbitrary monad with multiplication $\mu\colon\modality^2\Rightarrow\modality$ and unit $\eta\colon 1_{\cat D}\Rightarrow\modality$.
    It induces an adjunction between $\cat D$ and its category of algebras
    \begin{center}
      \begin{tikzcd}
        \cat D^\modality \arrow[r, shift right=2mm, "U"'{name=U}]&
        {\cat D}, \arrow[l, shift right=2mm, "L"'{name=L}]
        \arrow[phantom, from=L, to=U, "\dashv" rotate=-90]
      \end{tikzcd}
    \end{center}
    such that $UL\cong\modality$, where $L$ sends an object $d$ to the free algebra $\modality d$ on $d$ and $U$ simply forgets the algebra structure.
    The set of morphisms between two $\modality$-algebras is defined as a certain subset of the morphisms between their underlying objects in $\cat D$, which makes it obvious, that $U$ is faithful.

    Now suppose $\modality$ is idempotent, meaning $\mu$ is invertible.
    In this case the multiplication $\delta$ of any $\modality$-algebra $(d,\delta)$ is invertible as well, with inverse the unit component of the monad at $d$:
    By definition $\delta$ is a postinverse to $\eta_d$.
    On the other hand
    \begin{align}\label{eq:unit_idempotent_monad}
      \eta_d\cdot \delta=\modality\delta\cdot\eta_{\modality d}
                   =\modality \delta\cdot\modality \eta_d
                   =\modality (\delta\cdot\eta_d)
                   =1_{\modality d},
    \end{align}
    where the first equality follows from naturality and the second one from the fact, that both $\eta\modality$ as well as $\modality\eta$ are inverses of $\mu$ and therefore equal.
    Hence the multiplication of a $\modality$-algebra is equal to the inverse $\eta_d^{-1}$ of its unit component, so any morphism between the underlying objects of two $\modality$-algebras is already an algebra morphism by naturality of $\eta$.
    This proves, that $U$ is a fully faithful functor. 
    (Being a $\modality$-algebra is a property, rather than a structure!)

    From the above follows, that the unit component $\eta_d$ of an object $d\in\cat D$ is invertible if and only if $d$ lies in the essential image of $U$.
    It is also easy to see, that $d$ is $\modality$-local in this case.
    Suppose conversely that $d$ is $\modality$-local.
    Since the unit component of any object is $\modality$-local (as an inverse to multiplication $\mu$), the induced map
    \[\cat D(\modality d, d)\xrightarrow{\eta_d\pull}\cat D(d,d)\]
    then is an isomorphism.
    In particular the identity of $d$ has a postinverse $\delta$ under precomposition by $\eta_d$.
    But this fact, is the only assumption we needed for equation \ref{eq:unit_idempotent_monad} to hold, so we may once again deduce, that $\delta$ is also a preinverse to $\eta_d$.
    Therefore, $\eta_d$ is invertible and $d$ lies in the essential image of $U$.

    Finally if, in addition, $\modality$ is left exact, the reflector  $L$ is left exact as well.
    Because to check if the image under $L$ of a limit cone in $D$ is a limit cone in $D^\modality$, we may compose it with $U$, since $U$ reflects limits as a fully faithful functor.
    But $UL\cong\modality$ preserves finite limits, so $L$ preserves them as well.

    Conversely, given a reflective subcategory with left exact reflector
    \begin{center}
      \begin{tikzcd}
        \cat E \arrow[r, hook, shift right=2mm, "U"'{name=U}]&
        {\cat D}, \arrow[l, {Rays[]}->, shift right=2mm, "L"'{name=L}]
        \arrow[phantom, from=L, to=U, "\dashv" rotate=-90]
      \end{tikzcd}
    \end{center}
    its induced monad $UL$ on $\cat D$ certainly is left exact, since $U$, a right adjoint, preserves limits.
    Furthermore, since $U$ is full and faithful, the counit $\varepsilon$ of the adjunction is an isomorphism, which consequently also holds for the multiplication $U\varepsilon L$ of the monad, proving its idempotency. 
    We refer to \cite[Proposition 5.3.3]{riehl2017category} for a proof that $\cat E$ is equivalent to the category of $UL$-algebras.

    At last, let us address the issue, that the proposition spoke about reflective \emph{localizations}, while so far we only talked about reflective \emph{subcategories}.
    Recall, that a reflective localization is given by an adjunction of the form
    \begin{center}
      \begin{tikzcd}
        {\cat D[\cat W^{-1}]} \arrow[r, hook, shift right=2mm, "U"'{name=U}]&
        {\cat D}, \arrow[l, shift right=2mm, "L"'{name=L}]
        \arrow[phantom, from=L, to=U, "\dashv" rotate=-90]
      \end{tikzcd}
    \end{center}
    where $\cat W$ is a class of morphisms of $\cat D$ and $\cat D[\cat W^{-1}]$ satisfies a certain 2-categorical universal property with respect to functors sending morphisms in $\cat W$ to isomorphisms.
    In particular, it specifies the inclusion of a reflective subcategory of $\cat D$.
    Conversely any reflective subcategory with reflector $L$ satisfies the defining universal property of a reflective localization with respect to the class of those morphisms, that are sent to isomorphisms by $L$.
    Therefore it suffices to show, that this class is given by the $\modality$-local morphisms, in case $L$ is induced by a local operator $\modality$.
    But this readily follows from the fact, that $\modality\cong UL$ and $U$ is fully faithful and thus conservative.
  \end{proof}

It is not immediately clear from the above characterizations of a topos, why our example of the Zariski sheaves should form one.
We will address this issue later on, when we introduce another characterization, that is more convenient for direct constructions.

  But first, let us illustrate from this more abstract perspective, why we deem toposes an appropriate setting for geometry and physics.
  As our main interest in this topos theoretic approach to geometry lies in its application to field theory, and field theory is conventionally formalized in a smooth setting\todo{For reasons we will see later? PDEs!}, we are going to draw comparisons between arbitrary toposes and the category of smooth manifolds.
  In the following, let $\cat H$ be any topos over a site of definition $(\cat C,\modality)$.
    \begin{description}%[label=(\alph*),leftmargin=*]
      \item [Yoneda embedding]
        We regard objects of $\cat C$ as simple test spaces for our geometry and objects of $\cat H$ as spaces modeled or parametrized by those test spaces%
        \footnote{This the "gros topos" approach to geometry, in contrast to the "petit topos" approach, where a topos $\Sh(X)$ of sheaves on $X$ is regarded as a space itself, replacing the space $X$.}.
        The set of parametrizations of a space $X\in \cat H$ by a test space $c\in \cat C$ is by definition the value $X(c)$ of $X$ at $c$, where we regard $X$ as a contravariant functor from $\cat C$ to the category of sets via the embedding $\cat H\hookrightarrow \PSh(\cat C)$.
        Intuitively, test spaces should be spaces and parametrizations of a space $X$ by a test space $c$ should just be maps $c\to X$.
        We can formalize this by demanding the Yoneda embedding
        \begin{align*}
          \yo\colon \cat C&\longrightarrow \PSh(\cat C)\\
          c&\longmapsto \cat C(\--,c)
        \end{align*}
        to factorize (necessarily also by a fully faithful functor) through the inclusion of $\cat H$ into the category of presheaves over $\cat C$:
        \begin{center}
          \begin{tikzcd}
            \cat C \arrow[rr, hook, "\yo"{name=yo}] \arrow[dr] && {\PSh(\cat C)}\\
            &\cat H \arrow[ur, hook] \arrow[phantom, "\cong", from=yo] 
          \end{tikzcd}
        \end{center} 
        The Yoneda lemma implies that $X(c)\cong \cat H(c,X)$, as desired.

        Note that a site is not uniquely determined by its topos, there may be several inequivalent ones giving rise to equivalent toposes.\todo{Example?}
        In general, not every site will fulfill the above demand, however, for any given topos we can always find a site of definition that does, see \cite[Appendix, §4, Corollary 2]{maclane2012sheaves}.
        Since we put emphasis on the topos, not on the site, we may therefore assume without loss of generality, that $\cat H$ contains the test spaces on which it is modeled in a consistent way, meaning it fulfills the Yoneda lemma with respect to the test spaces.
        Also note that we will usually suppress the Yoneda embedding $\yo$ in the following, identifying a test space $c$ with its representable presheaf $\yo(c)$ respectively the corresponding space in $\cat H$.


      \item [Limits] 
        The inclusion of $\cat H$ into $\PSh(\cat C)$ creates all limits that exist in $\PSh(\cat C)$.
        This can be shown as a consequence of the inclusion being a monadic functor, i.e.~$\cat H$ being the category of algebras of a monad up to equivalence, compare \cite[Corollary 5.6.6]{Riehl2017category}. 
        Since any category of presheaves is complete, that is, all small limits exist in it, toposes are complete as well.
        Therefore we are, for example, able to form products of spaces in $\cat H$ and intersections of subspaces of a fixed space, properties not shared by the category of smooth manifolds in full generality.
        For instance, if two submanifolds of a smooth manifold intersect transversally, their pullback exists, but this must not be the case in a general situation.
        \todo{Example!}

      \item [Colimits]
        Similarly to the above case, any topos is also cocomplete, that is, all small colimits exist.
        Explicitly, the colimit of a diagram in $\cat H$ is constructed by forming the colimit in $\PSh(\cat C)$ and subsequently applying the reflector onto $\cat H$.
        The existence of colimits allows us to form disjoint unions and quotients of spaces.
        Like in the case of limits, the category of smooth manifolds does not permit these operations in general, e.g. one usually demands the action of a Lie group on a smooth manifold to be free and proper in order for the quotient to be a smooth manifold again.
        \todo{Example!}
        \todo{Something about effective equivalence relations? See [Riehl, Categories in context, p.103ff].}

      \item [Cartesian closure]
        Recall that a category $\cat H$ with finite products is \emph{cartesian closed} if for each object $X$ the functor $\--\times X$ has a right adjoint $[X,\--]$.
        For any $Y\in\cat H$ the \emph{internal hom} $[X,Y]$ of $X$ and $Y$ represents maps from $X$ to $Y$ in the sense that morphisms from a third object $T$ to $[X,Y]$ correspond to $T$-parametrized maps $T\times X\to Y$.
        In particular the "points" $1\to[X,Y]$ of the internal hom correspond to maps from $X$ to $Y$.
        This allows us to formalize the notion of a mapping space, a concept frequently encountered in physics, where mapping spaces arise as configuration spaces of physical systems.
        For example, if $R$ is a "worldline object" and $X$ a "spacetime object", the mapping space $[R,X]$ represents the space of trajectories of a moving particle in spacetime.
        We will prove below, that any topos is cartesian closed, a property not shared by the category of smooth manifolds.
        \\
        \begin{proposition}
          Any topos is cartesian closed.
        \end{proposition}
        \begin{proof}
          It is straightforward to prove this in the case of a presheaf topos $\PSh(\cat C)$ over a small category $\cat C$, where finite products are formed objectwise and the internal hom of two presheaves $X,Y$ is given by
          \[c\mapsto [X,Y](c)\coloneqq \cat C(c\times X,Y).\]

          The case of arbitrary toposes follows from the statement that the category of local algebras of any local operator $\modality$ on a cartesian closed category $\cat D$ is cartesian closed.
          Note that $\cat D^\modality$ is equivalent to the reflective subcategory of $\modality$-local objects of $D$ and it suffices to prove, that this subcategory is closed under forming finite products and internal homs.
          The first property we already addressed above when talking about limits.
          For the second one, we will prove more generally that the internal hom $[b,c]$ of two objects $b,c$ in $\cat D$ is $\modality$-local if $c$ is.
          In the proof of \ref{prop:local_operator_subcategory} we saw that an object is actually already $\modality$-local if and only if for each unit component $a\xrightarrow{\eta_a}\modality a$ the induced morphism $\cat D(\modality a,d)\xrightarrow{\eta_a\pull}\cat D(a,d)$ is a bijection (no need to test all $\modality$-local morphisms).
          
          Thus let $a,b,c$ be objects of $\cat D$ and $b$ be $\modality$-local.
          Proving that $\cat D(\modality a, [b,c])\xrightarrow{\eta_a\pull}\cat D(a,[b,c])$ is a bijection is equivalent to showing $\cat D(\modality a\times b,c) \xrightarrow{(\eta_a\times b)\pull}\cat D(a\times b,c)$ is one, due to the adjunction $\--\times b\dashv [b,\--]$.
          Because $c$ is $\modality$-local, this follows if $\eta_a\times b$ is $\modality$-local.
          To see this, observe that
          \begin{center}
            \begin{tikzcd}
              \modality(a\times b) \arrow[rr, "\modality(\eta_a\times b)"] \arrow[dd, "{(\modality\pr_a, \modality\pr_b)}"'] &&
              \modality(\modality a\times b) \arrow[dd, "{(\modality\pr_{\modality a},\modality\pr_b)}"]\\\\
              \modality a\times\modality b \arrow[rr, "\modality\eta_a\times\modality b"] &&
              \modality^2 a\times \modality b
            \end{tikzcd}
          \end{center}
          commutes.
          Since $\modality$ is left exact, the vertical maps are isomorphisms and since $\modality$ is idempotent, $\modality\eta_a$ is an isormorphism, so the lower horizontal morphism is an isomorphism.
          But then the upper horizontal morphism is one as well, proving $\eta_a\times b$ to be $\modality$-local.
        \end{proof}

      \item [Local cartesian closure]
        In generalization of the preceding paragraph, a category $\cat H$ with terminal object $1$ is called \emph{locally cartesian closed} if for all objects $B$ of $\cat H$ the slice categories $\cat H\slice B$ are cartesian closed.
        Recall that objects of the slice category $\cat H\slice B$ are represented by morphism $E\xrightarrow{p} B$ in $\cat H$ and morphisms in $\cat H\slice B$ are given by commutative triangles
        \begin{center}
          \begin{tikzcd}[column sep=small]
            E\arrow[rr]\arrow[dr, "p"'] && E'\arrow[dl, "p'" yshift=2pt]\\
            &B&
          \end{tikzcd}
        \end{center}
        in $\cat H$.
        Demanding the existence of a terminal object forces $\cat H$ itself to be cartesian closed, because $\cat H\slice 1$ is equivalent to $\cat H$.
        Let us denote the internal hom of a slice category $\cat H$

        Binary products in slice categories are given by pullbacks, so $\cat H$ possesses all pullbacks if it is locally cartesian closed.
        In any category with pullbacks a morphism $A\xrightarrow{f} B$ induces a functor $\cat H\slice B\xrightarrow{f\pull}\cat H\slice A$, sending an object of $\cat H\slice B$ represented by by a morphism $E\xrightarrow{p}B$ to the object represented by the pullback $E\times_B A\xrightarrow{f\pull(p)} A$.
        This functor has a left adjoint $\Sigma_f$ or $f\shriek$, which acts by postcomposition with $f$, sending $F\xrightarrow{q} A$ to $F\xrightarrow{q}A\xrightarrow{f}B$.
    \end{description}

      \todo[inline]{Topos of smooth sets provides an enlargement of the category of smooth manifolds, which satisfies the above desired properties. 
      There are other methods to deal with the issues raised above, for example orbifolds for quotients of smooth manifolds or Fréchet manifolds for mapping spaces.
      While these give more fine-grained results and "support more differential calculus" (in the case of Fréchet. What's the slogan for orbifolds?), the topos of smooth sets provides an encompassing environment making all constructions possible, but having "nastier" objects.
      Grothendieck: "It is better to work in a nice category with nasty objects than in a nasty category with nice objects."}

  \begin{definition}
    Sieves, topology, site, descent object, sheaf, sheaf topos.
  \end{definition}
  \begin{examples}
    \begin{enumerate}[label=(\alph*),leftmargin=*]
      \item Topos of smooth sets over site of Cartesian spaces.
      \item Cahiers topos of formal smooth sets over formal Cartesian spaces.
    \end{enumerate}
  \end{examples}

  Depending on the site of definition the flavour of geometry encoded by the corresponding topos changes.
  Above we sketched how sheaves on the Zariski site of affine schemes encompass schemes and thus encode algebraic geometry, while later on, in the section of this chapter, we introduce a site that is suited for differential geometry.
  The objects of this site will be abstract real coordinate systems, while morphisms will be smooth coordinate transformations between them.
  Therefore, the sheaves over this site, to be called smooth sets, are geometric objects that are parametrized by smooth coordinate systems.
  The category of smooth manifolds embedds fully into the topos of smooth sets, which is why we can consider this topos to be a reasonable extension of differential geometry.


  \section{$\infty$-Toposes from $\infty$-algebraic theories}

  \section{Presentation and models}

  \section*{Leftovers}
  \paragraph*{Slice toposes and jet bundles}
        More precisely, for $X\in \cat H$ an object, the slice category $\cat H_{/X}$ has as objects the morphisms $E\to X$ and as morphisms the commutative triangles
        \begin{center}
          \begin{tikzcd}
            E \arrow[rr] \arrow[dr] && E' \arrow[dl]\\
            &X
          \end{tikzcd}.
        \end{center}
        One can show \todo{Reference or proof} that $\cat H_{/X}$ is a topos with site of definition $\cat C_{/X}$, the category of elements%
        \footnote{The objects of $\cat C_{/X}$ are morphisms $c\to X$ in $\PSh(\cat C)$ for $c\in \cat C$, where we identify $\cat C$ with its image under the Yoneda embedding, while morphisms are given by the evident commutative triangles.
        Equivalently, by the Yoneda lemma, its objects can be described as pairs $(c,x)$, where $c\in \cat C$ and $x$ is an element of $ X(c)$, and morphisms $(c,x)\to (c',x')$ are maps $c\to c'$, such that the induced map $X(c')\to X(c)$ sends $x'$ to $x$, hence the name "category of elements".}
        of $X$.

        For another example, we use the fact that slice categories of toposes are toposes\todo{Proof later or reference}.
        If $X$ is a space in $\cat H$, we may think of the spaces in $\cat H_{/X}$ as bundles over $X$.
        The existence of limits then permits us to work with "(locally-)pro bundles", arising for example in the construction of infinite jet bundles of a fibered manifold $E\to X$.
        It is the limit of finite order jet bundles
        \begin{center}
          \begin{tikzcd}
            \cdots\arrow[r] & J^kE\arrow[r]\arrow[drr, ""{name=A}] & \cdots\arrow[r] & J^1E\arrow[r]\arrow[d, ""{name=B}] & J^0E\arrow[dl]\\
            &&& X 
        %    \arrow[phantom, from=A, to=B, "\cdots"]
          \end{tikzcd}.
        \end{center}
        This limit does not exist in the category of smooth manifolds, instead it is formed in the larger category of Fréchet manifolds.
        \todo{Reference to Khavkine/Schreiber or later part of thesis, where an embedding of these categories into an appropriate topos is given. The topos perspective is advantageous, since the definition of smooth maps on locally-pro bundles is more conceptual and therefore simpler.}

          To prove this, note that $\cat D^\modality$ is a reflective localization of $\cat D$ at the class of $\modality$-local morphisms.
          However, we may also describe it as the localization at a smaller class of morphisms, consisting only of the components of the unit $\eta$ of $\modality$.
          This is because any functor $F$ starting at $\cat D$ that sends the unit components to isomorphisms, also sends all $\modality$-local morphisms to isomorphisms:
          \begin{center}
            \begin{tikzcd}
              a \arrow[r, "f"]\arrow[d, "\eta_a"]& b\arrow[d, "\eta_b"{name=etab}] 
              && Fa\arrow[r,"Ff"]\arrow[d,"\sim"{rot}, ""'{name=Fetaa}]& Fb\arrow[d, "\sim" {rot}]\\
              \modality a \arrow[r, "\modality f"', "\sim"]& \modality b
              &&F\modality a\arrow[r, "\sim"]& F\modality b 
              \arrow[mapsto, from=etab, to=Fetaa, shorten=2mm, "F"]
            \end{tikzcd}
          \end{center}
