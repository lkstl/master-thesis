\section{Local operators and toposes}
In parametrized geometry we start with an arbitrary small category $\cat C$ and regard it as a category of \emph{model spaces} for our geometry.
Recall that a presheaf on $\cat C$ is a functor $\cat C\op\to\Sets$.
We understand such a presheaf $X$ as a \emph{generalized space} modeled on the objects of $\cat C$, in the sense that the value $X(c)$ of $X$ at a model space $c$ ought to be the set of all "geometric maps" $c\to X$, the set of "plots" of $X$ by $c$.
Any "actual geometric map", a morphism between model spaces, should give rise to a map between the sets of plots in the opposite direction, which is reflected by the contravariance of a presheaf.
The functoriality condition then ensures compatibility of this assignment with identities and composition in the category of model spaces.

Any model space $c$ gives rise to a generalized space $\cat C(\cdot,c)$ whose plots are the "actual geometric maps" into it.
This assignment of a model space to its associated generalized space is functorial, that is, we obtain the \emph{Yoneda functor} $\yo$ from $\cat C$ to the category $\PSh(\cat C)$ of presheaves over $\cat C$.
For the sake of consistency, when mapping between model spaces, it should not matter whether we view them as objects of $\cat C$ or as their associated generalized spaces.
Furthermore, the plots $X(c)$ of a generalized space $X$ with regard to a model space $c$ should coincide with the maps $\cat C(\cdot,c)\to X$ of generalized spaces.
These two consistency conditions are dealt with by the Yoneda lemma.

\begin{lemma}[Yoneda]
  Let $\cat C$ be a small category and $\PSh(\cat C)$ be the category of presheaves over it.
  The Yoneda functor is defined by
  \begin{center}
    \begin{tikzcd}[row sep=0.5ex]
      \cat C \arrow[r, "\yo"] & \PSh(\cat C)\\
      c\arrow[dd, shorten=1pt, "f"'{scale=0.9}] & {\cat C(\cdot,c)}\arrow[dd, "f\push"{scale=0.9, yshift=1.5pt}]\\
      {} \arrow[r, mapsto, shorten=3ex]& {}\\
      c' & {\cat C(\cdot,c')}
    \end{tikzcd}.
  \end{center}
  For any presheaf $X$ and any object $c$ of $\cat C$ there is a bijection
  \begin{align*}
    \PSh(\cat C)(\yo(c),X) &\longrightarrow X(c)\\
    \varphi &\longmapsto \varphi_c(1_c)
  \end{align*}
  which is natural in $c$ and in $X$.
  As a consequence, the Yoneda functor $\yo$ is full and faithful, hence we call $\yo$ the \emph{Yoneda embedding} of $\cat C$ in $\PSh(\cat C)$ from now on.
\end{lemma}

We usually suppress the Yoneda embedding and instead write $c$ for both the model space and its associated generalized space.

