\section{Idempotent monads and reflective localizations}

An important class of examples for (co)monadic functors is given by inclusions of (co)reflective subcategories.


\begin{definition}
  Let $\cat E \xhookrightarrow{\iota}\cat D$ be a full and faithful functor.
  If $\iota$ has a left (right) adjoint $L$, we call $\cat E$ a \emph{(co)reflective subcategory} of $\cat D$ and $L$ the \emph{(co)reflector}.
\end{definition}


\begin{proposition}
  The inclusion of a (co)reflective subcategory is (co)monadic.
\end{proposition}
\begin{proof}
  We will consider the inclusion of a reflective subcategory $\reflectivesub{\cat E}{L}{\iota}{\cat D}$ only, the coreflective case being formally dual.
  By the Beck monadicity theorem \ref{theorem:Beck} we have to prove that the right adjoint functor $\iota$ creates coequalizers of $\iota$-split pairs $\arrowpair{e}{f}{g}{e'}$.
  However, since $\iota$ is full and faithful and therefore reflects all colimits, it suffices to show, that $\iota$ lifts $\iota$-split coequalizers.
  Let
  \begin{center}
    \begin{tikzcd}
      \iota e \arrow[r, "\iota f", yshift=0.5ex] \arrow[r, "\iota g"', yshift=-0.5ex] & \iota e' \arrow[r, "h"]\arrow[l, "t", bend left=40] & d \arrow[l, "s", bend left=40]
    \end{tikzcd}
  \end{center}
  be a split coequalizer.
  Its image under $L$ is also a split coequalizer and since $\iota$ is full and faithful, the counit $L\iota\xrightarrow{\epsilon} 1_{\cat E}$ is an isomorphism and we obtain a split coequalizer
  \begin{center}
    \begin{tikzcd}
      e \arrow[r, "f", yshift=0.5ex] \arrow[r, "g"', yshift=-0.5ex] & e' \arrow[r, "Lh\circ \epsilon_{d'}^{-1}"]\arrow[l, "\epsilon_d\circ t\circ \epsilon_{d'}^{-1}", bend left=40] & Ld \arrow[l, "\epsilon_{d'}\circ Ls", bend left=40]
    \end{tikzcd}.
  \end{center}
  Again, its image under $\iota$ is split and the coequalizer is thus preserved by $\iota$.
\end{proof}


In the proof we used that the counit of an adjunction with full and faithful right adjoint is an isomorphism.
In particular the multiplication of the induced monad becomes an isomorphism.
Dually, the comultiplication of a comonad coming from an adjunction with full and faithful left adjoint is an isomorphism.

\begin{definition}
  A (co)monad is called \emph{idempotent} if its (co)multiplication is an isomorphism.
  We usually denote idempotent (co)monads with the symbol $\modality$.
\end{definition}


Any (co)reflective subcategory gives rise to an idempotent (co)monad, which begs the question whether the converse is also true.
Let us make two quick observations about idempotent monads $\modality$ before answering the question:
\begin{enumerate}
  \item By the defining property of the unit $\eta$ of $\modality$, both $\modality\eta$ and $\eta\modality$ are inverse natural transformations to the invertible multiplication of $\modality$, in particular, they are equal.
  \item If $(a,\alpha)$ is a $\modality$-algebra, $\eta_a$ is a preinverse to $\alpha$ by definition.
    However, using the above observation, we see that
    \[\eta_a\cdot\alpha
    =\modality\alpha\cdot\eta_{\modality a}
    =\modality\alpha\cdot\modality\eta_a
    =\modality(\alpha\cdot\eta_a)
    =1_{\modality a},\]
    making $\eta_a$ a postinverse to $\alpha$ and thus $\alpha=\eta_a^{-1}$.
    In particular any $\modality$-algebra is isomorphic to its image under $\modality$ and thus free.
    Conversely, if the unit component of an object $a$ is invertible, its inverse endows $a$ with the structure of a (free) $\modality$-algebra.
\end{enumerate}
There are obvious dual versions of these observations for idempotent comonads.

\begin{proposition}
  The Eilenberg--Moore category of an idempotent (co)monad on a category $\cat D$ is a (co)reflective subcategory of $\cat D$.
\end{proposition}
\begin{proof}
  We consider the case of an idempotent monad $\modality$ on a category $\cat D$ with corresponding Eilenberg--Moore category $\adjunction{\cat D^\modality}{F^\modality}{U^\modality}{\cat D}$.
  By construction, the functor $U^\modality$ is already faithful.
  By the second observation above, there is at most one structure of a $\modality$-algebra on each object of $\cat D$, meaning a $\modality$-algebra is an object with a property (its unit component being invertible), rather than an object with a structure.
  This suggests that the forgetful functor $U^\modality$ is also full, since it forgets only properties rather than structures.
  To prove this formally, note that the following square commutes by naturality of $\eta$ for all $\modality$-algebras $(a,\eta_a^{-1})$ and $(a',\eta_{a'}^{-1})$ with a morphism $a\xrightarrow{f} a'$ between their underlying objects in $\cat D$:
  \begin{center}
    \begin{tikzcd}
      \modality a \arrow[r, "\modality f"] \arrow[d, "\eta_a^{-1}"'] & \modality a' \arrow[d, "\eta_{a'}^{-1}"]\\
      a \arrow[r, "f"] & a'
    \end{tikzcd}
  \end{center}
  But this means, that $f$ is in fact already a morphism of algebras, making $U^\modality$ a full functor.
\end{proof}

We now have to perspectives on the same concept, (co)reflective subcategories and idempotent (co)monads, and saw that the essential image of a (co)reflective subcategory consists of exactly those objects whose unit component is invertible, the ((co)free) (co)algebras of the induced idempotent (co)monad.

We want to introduce another perspective on (co)reflective subcategories.
For this, recall the notion of localization of a category $\cat D$ at a class of morphisms $\cat W$ of $\cat D$.
This is a category $\cat D[\cat W^{-1}]$ together with a functor $\cat D\xrightarrow{L}\cat D[\cat W^{-1}]$ sending all morphisms in $\cat W$ to isomorphisms, such that the following 2-universal property is satisfied:
For any functor $\cat D\xrightarrow{F}\cat C$ sending all morphisms in $\cat W$ to isomorphisms, there exists a functor $\cat D[\cat W^{-1}]\xrightarrow{\bar{F}}\cat C$ together with a natural isomorphism $F\xRightarrow{\psi}\bar{F}L$. 
This datum is unique up to a unique natural isomorphism, in that, given another functor $\cat D[\cat W^{-1}]\xrightarrow{\bar{F}}'\cat C$ and a natural isomorphism $F\xRightarrow{\psi'}\bar{F}'L$, there is a unique natural transformation (necessarily an isomorphism) $\bar{F}\xRightarrow{\theta}\bar{F}'$ such that $\theta\psi=\psi'$.
\begin{center}
  \begin{tikzcd}[row sep = huge, column sep = large]
    \cat D \arrow[rr, "F", ""'{name=F}, ""'{name=Fstart, near start}] \arrow[d, "L"'] && \cat C\\
    {\cat D[\cat W^{-1}]} \arrow[urr, "\bar{F}'" description, ""{name=B, near start}, bend right=35, ""{name=Bmid}] 
    \arrow[Rightarrow, from=Fstart, start anchor={[xshift=-1ex]}, to=B, gray, "\psi'" near start, "\sim"'{near end, rotate=-70, scale=0.8, yshift=-0.5ex}, bend left =20]
    \arrow[urr, "\bar{F}" description, ""{name=A, near start}, bend left=10, crossing over, ""'{name=Amid}]
    \arrow[Rightarrow, from=Fstart, start anchor={[xshift=-2ex]}, to=A, gray, "\psi"' near start, "\sim"{near start, rotate=-65, scale=0.8}, bend right=10]
    \arrow[Rightarrow, from=Amid, to=Bmid, gray, "\exists !\theta"]
  \end{tikzcd}
\end{center}
The idea is, that we want to make the morphisms in $\cat W$ invertible and the localization is the universal (2-initial) way of achieving this goal.
In case the localization functor $L$ has a full and faithful right (left) adjoint $\iota$ we call $\lrhookadj{\cat D[\cat W^{-1}]}{L}{\iota}{\cat D}$ a \emph{(co)reflective localization} of $\cat D$ at $\cat W$.
By definition any (co)reflective localization of a category $\cat D$ gives rise to a (co)reflective subcategory of $\cat D$, however, the converse is also true:

\begin{proposition}
  A (co)reflective subcategory  $\lrhookadj{\cat E}{L}{\iota}{\cat D}$ is a (co)reflective localization of $\cat D$ at one of the following classes of morphisms:
  \begin{enumerate}
    \item The class of $L$-local morphisms%
      \footnote{Given a functor $\cat D\xrightarrow{L}\cat E$, a morphism $a\xrightarrow{f} b$ is called \emph{$L$-local} if its image under $L$ is an isomorphism.}.
    \item The class of $\modality$-local morphisms, where $\modality$ is the induced idempotend (co)monad.
    \item The class consisting of components of the adjunction (co)unit.
  \end{enumerate}
\end{proposition}
\begin{proof}
  Since $\iota$ is a full and faithful functor, it preserves and reflects isomorphisms, which shows that the first two classes are equal.
  Furthermore, we already saw in out observations above that the components of the (co)unit of the (co)monad $\modality$ are $\modality$-local.
  Conversely, any functor $\cat D\xrightarrow{F}\cat C$ that maps all (co)unit components of $\modality$ to isomorphisms, already has this property with regards to all $\modality$-local morphisms $a\xrightarrow{f} b$.
  For example, in case $\modality$ is an idempotent monad, this can by seen by considering the following diagram, which is the image of the naturality square for the unit $\eta$:
  \begin{center}
    \begin{tikzcd}
      Fa \arrow[r, "Ff"] \arrow[d, "F\eta_a"', "\sim"{rotate=90, xshift=-1ex, yshift=-0.75ex}] & Fb \arrow[d, "F\eta_b", "\sim"'{rotate=90, xshift=1.25ex, yshift=0.5ex}]\\
      F\modality a \arrow[r, "F\modality f"', "\sim"] & F\modality b
    \end{tikzcd}
  \end{center}
  Since three of the four morphisms are isomorphisms, the fourth one, $Ff$, is one as well, proving our claim.
  In conclusion, a localization at the third class is also a localization at the second class and vice versa.
  Finally it remains to prove, that the universal property of the localization really is satisfied, for which we refer to \todo{Insert reference to [Categories and toposes, Proposition 1.77]}.
\end{proof}

The localization perspective gives us another way of describing the essential image of a (co)reflective subcategory, namely as the full subcategory of (co)local objects for the class $\cat W$ of morphisms, we wish to invert in the localization process.
A (co)local object behaves as if the morphisms in $\cat W$ were already invertible in $\cat D$, with respect to mapping into (out of) it.

\begin{definition}
  Let $\cat W$ be a class of morphisms of a category $\cat D$.
  An object $d$ of $\cat D$ is called \emph{$\cat W$-(co)local} if the map $\cat D(b,d)\xrightarrow{f\pull}\cat D(a,d)$ (respectively $\cat D(d,a)\xrightarrow{f\push}\cat D(d,b)$) is a bijection for all morphisms $a\xrightarrow{f} b$ in $\cat W$.
  In the special case that $\cat W$ is a class of $L$-local morphisms for some functor $L$ the $W$-(co)local objects are called \emph{$L$-(co)local}.
\end{definition}


\begin{proposition}
  Let $\lrhookadj{\cat D[\cat W^{-1}]}{L}{\iota}{\cat D}$ be a (co)reflective localization of a category $\cat D$ at a class $\cat W$ of morphisms.
  The essential image of the inclusion functor $\iota$ is the full subcategory of $\cat W$-(co)local objects.
\end{proposition}
\begin{proof}
  \todo[inline]{Reference to [Categories and toposes, Proposition 1.81].}
\end{proof}
